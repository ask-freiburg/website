---
title: Unsere Ziele
background: /assets/images/bg/spaziergang.jpg
menu:
  main:
    identifier: ziele
    weight: 1
slug: ziele
---
{{< image src="goal-icon.png" alt="Ziel-Symbol" width=200 width-mobile=150 float="right" margin="" no-resize="" >}}

- **Krisendienst verfügbar machen:** Wir setzen uns im Gemeinde-Psychiatrischen Verbund (GPV) für die Schaffung eines ambulanten Krisendienstes ein und wollen diesen mit gestalten. Damit soll ein Angebot zur ambulanten Begleitung von Menschen in psychischen Krisen etabliert werden.
- **Netzwerkgespräche in die psychiatrische Praxis bringen:** Wir organisieren Fortbildungen zum Offenen Dialog, führen Info-Veranstaltungen durch und betreuen einen Moderator:innen-Pool zur Vermittlung von Netzwerkgesprächen. Damit wird der Ansatz des Offenen Dialog ein Stück gelebte Praxis.
- **Verständnis von Krise und Krisenbegleitung kultivieren:** Wir bieten Krisenbegleitungs-Schulungen an, veranstalten Filmvorführungen und Vorträge und nehmen an Fachtagen teil. Damit wollen wir unsere Vision von Krisenbegleitung und unser Krisenverständnis verbreiten und diskutieren.

### Wie wir uns einen Krisendienst vorstellen

1. ein Krisentelefon für den Freiburger Raum, das rund um die Uhr für Betroffene erreichbar ist
2. eine Anlaufstelle, die auch abends und am Wochenende geöffnet ist und  die Möglichkeit zu einem persönlichen Kontakt bietet
3. der Einsatz von mobilen Krisenteams, die betroffene Menschen in ihrem häuslichen Umfeld begleiten, und die sich nach den Grundsätzen der {{< filelink src="V. Aderhold - Bedürfnisangepaßte Behandlung & Offener Dialog.pdf" text="„bedürfnisangepassten Behandlung“ und des „offenen Dialogs“" >}} richten.
4. Einbeziehung des sozialen Netzwerks der Betroffenen oder ggfs. der Aufbau eines solchen
5. Einbeziehung von Erfahrungsexperten (Peers, EXIN, Ehrenamtliche) in die Krisenbegleitung
6. eine Krisenwohnung mit Begleitung als Rückzugsmöglichkeit

## Aktivitäten

Wir arbeiten zum einen an Konzepten für die obigen Ziele und suchen immer wieder nach Möglichkeiten und Anknüpfungspunkten, wie diese finanziell, personell und mit passenden Räumlichkeiten Realität werden können. Daneben tragen wir auch dazu bei, dass Know-How für die Grundsätze der {{< filelink src="V. Aderhold - Bedürfnisangepaßte Behandlung & Offener Dialog.pdf" text="„bedürfnisangepassten Behandlung“ und des „offenen Dialogs“" >}} im Raum Freiburg entsteht. Und außerdem schaffen wir Anregungen und Raum für Austausch zum Verständnis von seelischen Krisen.

Unsere bisherigen Aktivitäten umfassen unter anderem:

* Konzepte für außerstationäre Krisenbegleitung erarbeiten und Umsetzungsmöglichkeiten suchen
* Gründung und Mitarbeit in der Arbeitsgruppe „Ambulante Krisenbegleitung“ im Gemeindepsychiatrischen Verbund (GPV), wo wir zusammen mit anderen Akteuren die Einrichtung eines ambulanten Krisendienstes für den Raum Freiburg auf den Weg bringen wollen
* Initiation und Teilnahme beim Fachtag GPV "Ambulant in Krisen begleiten" 
* Filmvorführungen z.B. von "Crazywise". "20 Jahre Trialog", "Himmel und Mehr"
* Organisation der [Fortbildung zum Offenen Dialog](/posts/open-dialogue-fortbildung-in-freiburg-2019/)
* Konzeption und Durchführung von Schulungen zum Thema Krisenbegleitung
* Moderation von Netzwerkgesprächen mit der Methode des offenen Dialog in der Region
* Aufbau eines Moderator:innen-Pools für Netzwerkgespräche im Raum Freiburg
* Infoveranstaltungen zum offenen Dialog
* Gründung der Regio-Krisendienst gGmbH zusammen mit FHG e.V. und Start e.V., die mit Hilfe einer Anschubfinanzierung von Aktion Mensch einen Krisendienst in der Region aufbaut
