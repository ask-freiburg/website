---
title: Seite nicht gefunden
---
{{< image src="404.png" alt="Symbol HTTP 404" width=150 width-mobile=100 float="right" >}}

Die von Ihnen aufgerufene Seite wurde nicht gefunden. Das heißt normalerweise

1. Falls Sie von einer externen Seite hierher kommen: Auf dieser Seite ist ein veralteter Verweis enthalten. Bitte informieren Sie den Betreiber der Seite, von der aus Sie hierher verwiesen wurden.
2. Falls Sie von einer Seite auf ask-freiburg.net hierher kommen: Da ist ein Fehler auf unserer Website. Bitte [informieren Sie uns]({{< relref "/pages/impressum/index.md" >}}), damit wir ihn korrigieren können!

vielen Dank dafür, ansonsten nutzen Sie unsere Seite gerne weiter über das Hauptmenü oben.
