---
title: Newsletter
background: /assets/images/bg/stuhlkreis.jpg
slug: newsletter-info
---
{{< image src="Human-emblem-mail-rectangle.png" alt="Newsletter-Symbol" width=200 width-mobile=150 float="right" >}}

Mit unserem Newsletter informieren wir regelmäßig (etwa vierteljährlich) über Neues von ask!. Darin findest du auch Hinweise auf Veranstaltungen und Schulungen. Zu unserem Leserkreis gehören Mitarbeiter\*innen aus psychiatrischen Arbeitsfeldern, Psychiatrieerfahrene und deren Angehörige, engagierte Bürger\*innen und weitere Interessierte. Wir freuen uns, auch dich in den Leserkreis mit aufzunehmen, falls du Interesse hast.

## Newsletter Anmeldung
Um unseren Newsletter zu erhalten, schreibe bitte formlos an {{< email "listen@ask-freiburg.net" >}}.

## Newsletter online lesen
Der letzte Newsletter kann hier gelesen werden: {{< current-newsletter >}}

Frühere Newsletter findest du in unserem [Newsletter-Archiv](/newsletters).
