---
title: ask! Fortbildung Offener Dialog in Freiburg
background: /assets/images/bg/stuhlkreis.jpg
menu:
  main:
    name: Fortbildung Offener Dialog
    identifier: fortbildung-offener-dialog
    weight: 4
slug: fortbildung-offener-dialog
layout: fortbildung
draft: false
---
**>> ab Oktober 2025 | Freiburg** \
Anmeldung: {{< filelink src="od-fortbildung-fr-anmeldformular.pdf" text="Anmeldebogen (PDF)" >}}\
{{< filelink src="infoblatt-od-freiburg-2025.pdf" text="Infos zur Fortbildung Offener Dialog Freiburg (PDF)" >}}

## Systemische Fortbildung: Offener Dialog - Sozialraumorientierte Netzwerkarbeit
Die Fortbildung kann nun auch 2025 nochmal in Freiburg angeboten werden! Die Weiterbildung wird ausgerichtet durch ask! e.V. Freiburg und startet im Oktober 2025.

## Hintergründe
Der Offene Dialog steht in der Tradition des „Bedürfnis-angepassten Behandlungsmodells“ der finnischen und skandinavischen Psychiatrie. Dabei bilden **Netzwerkgespräche, d.h. therapeutische Arbeit mit dem sozialen System der Klienten** - von Anfang an und möglichst kontinuierlich - das zentrale therapeutische Element. Die therapeutische Arbeit ist dabei weitgehend flexibel und an den Bedürfnissen, Ressourcen und eigenen Lösungen der Hilfesuchenden orientiert. Die konsequente Anwendung dieser Begleitungsprinzipien kann zur Verbesserung der regionalen Versorgungsstruktur beitragen und die fortschreitende ambulante Ausrichtung des Helfersystems einer Region erleichtern. Im Rahmen ambulanter sozialpsychiatrischer Settings (Eingliederungshilfe SGB IX) bewirkt dieses Vorgehen eine deutliche Verringerung der Hospitalisierung, verbesserte Krisenbewältigung im Lebensfeld und längerfristige Krisenprävention. Sie dient zudem einer kooperativen Vernetzung mit anderen Angeboten und Anbietern und fördert die Teamentwicklung und kooperative Kompetenz der Mitarbeiter.

## Inhalte
Die Fortbildung vermittelt vor allem die für die Netzwerkarbeit spezifische systemisch-**dialogische Gesprächskompetenz**. Darüber hinaus liegt der Fokus auf der Entwicklung und Vertiefung einer **therapeutischen Grundhaltung**, die von der Annahme einer fortlaufenden dialogischen Konstruktion von Wirklichkeit, unaufhebbarer Vielstimmigkeit sozialer Netzwerke, notwendiger Toleranz von Unsicherheit sowie Prozess- und Ressourcenorientierung gekennzeichnet ist.
Im Zentrum der Vermittlung stehen **Rollenspiele in Kleingruppen**, in denen das vermittelte Wissen eingeübt und in eigenen Netzwerkgesprächen erprobt wird. **Eine Anwendung im Arbeitsalltag setzt die häufige Zusammenarbeit von zwei Mitarbeitern und bei Krisen auch vorübergehend weiteren Teammitgliedern voraus. Entsprechend werden Anmeldungen von mindestens 2 Personen aus einem Team bevorzugt.**
Die Fortbildung wird geleitet von Dr. med. Volkmar Aderhold, FA für Psychiatrie und Psychotherapie, gemeinsam mit weiteren-Trainer:innen.

## Zeitlicher Umfang und Programm
Die Weiterbildung umfasst 9 Workshops á 2 Tage jeweils Mittwochs und Donnerstags von 9-17 Uhr mit insgesamt 144 Unterrichtsstunden.

Der Kurs schließt mit einem **Zertifikat** ab. Das Vollzertifikat setzt die aktive Teilnahme an 80% aller Seminartage voraus. Versäumte Seminare können kostenfrei in anderen Regionen nachgeholt werden. Die Fortbildung wird für anschließende systemische Ausbildungen an Instituten der DGSF anerkannt.


### Workshop 1 -  22./23. Oktober 2025

* Modell des Offenen Dialogs
* Modelle der Ambulantisierung
* Beobachtung 2. Ordnung
* Reflektierendes Team

### Workshop 2 -  26./27. November 2025

* Reflektieren in „Fall“besprechungen
* Soziale Netzwerkkarte
* Reflektieren in Klientengesprächen
* Reframing
* Umsetzung im Alltag

### Workshop 3 -  21./22. Januar 2026

* Netzwerkgespräche als offener Prozess
* Phasen und Dynamik von Netzwerkgesprächen
* Theorie der Veränderung durch Dialoge und Begegnungen
* Prozessorientierte Fragen
* Matrix eines Netzwerkgesprächs
* Innere Polyphonie
* Anlässe + Indikationen für Netzwerkgespräche

### Workshop 4 -  11./12. März 2026

* Sprache als zentrale Komponente
* Aktives Zuhören und Worte der Klient:innen
* Lebensgeschichte hinter den Symptomen
* Worte und ihre biographische Bedeutung
* Schlüsselelemente des Offenen Dialogs I
* Metakommunikation
* Eigene Angst vor Netzwerkgesprächen

### Workshop 5 -  15./16. April 2026

* Schlüsselelemente des Offenen Dialogs II
* Zirkuläre und antizipatorische Fragen I
* Strategien für schwierige Netzwerkgespräche
* Angst der Klient:innen vor Netzwerkgesprächen



### Workshop 6 -  24./25. Juni 2026

* Netzwerkgespräche in Krisen
* Zwischenbilanz
* Live-Netzwerkgespräch
* Erfahrungen mit eigenen Krisen
* Konstrukt Psychose im Offenen Dialog
* Netzwerkgespräche in psychotischen Krisen

### Workshop 7 - 23./24. September 2026

* Schlüsselelemente des Offenen Dialogs III
* Umgang mit Geheimnissen
* Live-Netzwerkgespräch
* Reflektieren nach Eskalationen
* Einladung der Leitungen
* Antizipatorische Fragen II
* Eigene negative Gefühle in den Kontakt bringen

### Workshop 8 - 28./29. Oktober 2026

* Krisenplan im Netzwerkgespräch entwickeln
* Netzwerke aktivieren
* Live-Netzwerkgespräch

### Workshop 9 - 25./26. November 2026

* Familien mit Kindern und Jugendlichen
* Kraft der Fragen
* Familienbrett
* Vertiefung der eigenen Kompetenz
* Eigene Bilanz

Diese Infos zur Fortbildung gibt es auch hier im druckbaren Format zum Download: {{< filelink src="infoblatt-od-freiburg-2025.pdf" text="Infos zur Fortbildung Offener Dialog Freiburg (PDF)" >}}

## Kosten und Rücktrittsrecht
2.430 € inkl. Fortbildungsmaterialien, Getränken, Snacks & Mittagessen. 

Kursstart bei einer Mindest-Anzahl von 26 Personen.

Der / die Kursteilnehmer:in kann von der Weiterbildungsmaßnahme bis zu einen Monat vor Beginn zurücktreten. Danach werden im Falle einer Kündigung 70% der Teilnahmegebühren für die verbleibenden Kurstage fällig, sofern der Platz nicht durch einen anderen Teilnehmer besetzt wird. In beiden Fällen ist eine schriftliche Kündigung erforderlich.


## Referent

**Volkmar Aderhold**

Dr. med. Volkmar Aderhold, FA für Psychiatrie und Psychotherapie sowie weitere Co-Trainer:innen 

## Kontakt, Informationen
> **ask! Außerstationäre Krisenbegleitung e.V.** \
> c/o Freiburger Hilfsgemeinschaft e.V. \
> Schwarzwaldstr. 9 \
> 79117 Freiburg
>
> **E-Mail:** {{< email "fortbildung@ask-freiburg.net" >}}

## Schulungsort
> **Bürgerhaus Zähringen** \
> Lameystr. 2 \
> 79108 Freiburg 

## Anmeldung
Für eine Anmeldung nutzen Sie bitte den {{< filelink src="od-fortbildung-fr-anmeldformular.pdf" text="Anmeldebogen (PDF)" >}}.

## Danke für die Unterstützung
* Wir bedanken uns herzlich für die finanzielle Unterstützung der Waisenhausstiftung Freiburg!
* Wir bedanken uns herzlich bei unserem Förderpartner Aktion Mensch!
{{< image src="aktion-mensch.png" alt="gefördert durch die AKTION MENSCH" width=291 width-mobile=200 float="" margin="4" no-resize="" >}}

## Archiv: Infoblatt der Fortbildung 2023/24
{{< filelink src="infoblatt-od-freiburg-2023.pdf" text="Infos zur Fortbildung Offener Dialog 2023/24 (PDF)" >}}

