---
title: Über uns
background: /assets/images/bg/stuhlkreis.jpg
slug: startseite
---
{{< image src="ask-logo.png" alt="ask! Logo" width=200 width-mobile=180 float="right" margin="" no-resize="" >}}

Wir sind eine Gruppe von Krisenerfahrenen, Angehörigen und Professionellen. Wir arbeiten daran, Begleitung und Unterstützung für Menschen in seelischen Krisen bereit zu stellen.

Unser Verständnis von Krise stützt sich auf die subjektive Sichtweise der Betroffenen und auf Erfahrungen aus der Selbsthilfe. Wir sehen Krise nicht primär als Krankheit, sondern als Prozess, dem die Möglichkeit zu persönlicher Entfaltung und Weiterentwicklung inne wohnt. Ob die in ihr liegende Chance zu einem entwicklungsfördernden Wendepunkt werden kann, hängt entscheidend von der Art und Weise des Umgangs mit einer Krise ab. 

“Soteria”, “Windhorse” und “Offener Dialog” sind weltweit erfolgreiche Ansätze und dienen uns als Grundlage unserer Arbeit. Ein wichtiger Bestandteil des Projekts sind Fortbildungen in diesem Bereich.

Wir sind Mitglied im Paritätischen Wohlfahrtsverband.

{{< filelink src="ask-flyer2_bildschirm.pdf" text="ask! Flyer" >}}
