---
title: Impressum
background: /assets/images/bg/stuhlkreis.jpg
menu:
  footer:
    name: Impressum/Datenschutz
    weight: 4
slug: impressum
---
{{< image src="logo-ask-schrift.jpg" alt="ask! Logo" width=200 width-mobile=150 float="right" margin="" no-resize="" >}}

{{< address >}}

Eingetragen im Vereinsregister Amtsgericht Freiburg unter der Registernummer VR 701299

Vertreten durch die Vorstände

* Uta Hempelmann
* Mirko Ološtiak-Brahms
* Gabriele Schmidt
* Jannis Seyfried
* Hanna Wagener 

## Datenschutz

Nach der EU-Datenschutz-Grundverordnung (DSGVO) sind wir verpflichtet, Sie darüber zu informieren, zu welchem Zweck Daten erhoben, gespeichert oder weiterleitet werden:\
Der Betreiber dieser Webseite speichert und verarbeitet generell keine personenbezogenen Daten. Es werden keine Cookies benutzt. Die Webseite wird von on [GitLab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) gehostet; möglicherweise speichert und verarbeitet GitLab Ihre IP-Adresse oder weitere Daten. Genaueres können Sie der [Datenschutzerklärung von GitLab](https://about.gitlab.com/privacy/) entnehmen. Für zeitweise geschaltete Anmeldeformulare oder Ähnliches können andere Datenschutz-Bestimmungen gelten, wobei dann an diesen Stellen in einer eigenen Datenschutzerklärung direkt über die Datails der Datenerhebung und -speicherung informiert wird. 

## Urheberrecht

Die durch die Seitenbetreiber erstellten Texte auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die erstellten Bilder sind, soweit nicht anders gekennzeichnet, lizenziert unter Creative-Commons-Lizenz [„Namensnennung – Weitergabe unter gleichen Bedingungen 4.0 international“](https://creativecommons.org/licenses/by-sa/4.0/deed.de), die Quellen können ggfs. dem [Bildnachweis](#Bildnachweis) entnommen werden.

Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet, für Bilder unter anderem auch beim [Bildnachweis](#Bildnachweis). Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen.

## Streitschlichtung

Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.

## Bildnachweis

* Wikimedia Commons contributors, "File:Proposal icon.svg," *Wikimedia Commons, the free media repository,* <https://commons.wikimedia.org/w/index.php?title=File:Proposal_icon.svg&oldid=303418154> (accessed July 8, 2020).
* Wikimedia Commons contributors, "File:Wikimedia Deutschland icon donate green.png," *Wikimedia Commons, the free media repository,* <https://commons.wikimedia.org/w/index.php?title=File:Wikimedia_Deutschland_icon_donate_green.png&oldid=390643235> (accessed July 9, 2020).
* Wikimedia Commons contributors, "File:Human-emblem-handshake.svg," *Wikimedia Commons, the free media repository,* <https://commons.wikimedia.org/w/index.php?title=File:Human-emblem-handshake.svg&oldid=505739528> (accessed December 17, 2020).
* Wikimedia Commons contributors, "File:Human-emblem-web.svg," *Wikimedia Commons, the free media repository*, <https://commons.wikimedia.org/w/index.php?title=File:Human-emblem-web.svg&oldid=483421976> (accessed December 23, 2020).
* Wikimedia Commons contributors, "File:Human-emblem-mail.svg," *Wikimedia Commons, the free media repository*, <https://commons.wikimedia.org/w/index.php?title=File:Human-emblem-mail.svg&oldid=469836741> (accessed January 25, 2021).
* Bild "Crazy-Wise-Film": &copy; CRAZYWISE LLC <https://crazywisefilm.com/>
* Logo: &copy; Katrin Birke <https://www.birke-web-grafik.de/>

## Technische Umsetzung

Diese Website ist ein Open-Source-Projekt: <https://gitlab.com/ask-freiburg/website/>

Technische Umsetzung und Design: Jannis Seyfried
