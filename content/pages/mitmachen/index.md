---
title: Mitmachen
background: /assets/images/bg/stuhlkreis.jpg
menu:
  main:
    identifier: mitmachen
    weight: 2
slug: mitmachen
aliases:
  - /spenden
---
{{< image src="Human-emblem-handshake.png" alt="Mitmachen-Symbol" width=200 width-mobile=150 float="right" >}}

Vielleicht überlegst du, bei uns mitzumachen, weil dir unsere Ziele gefallen. Wenn du

- Lust hast, etwas über Krisenbegleitung und [Offenen Dialog](/netzwerkgespraeche) zu lernen
- Interesse hast, an einer Fortbildung in der Moderation des [Offenen Dialog](/netzwerkgespraeche) teilzunehmen
- dich für einen ambulanten Krisendienst im Raum Freiburg einsetzen möchtest
- dich in eine selbstorganisierte Gruppe einbringen möchtest

dann bist du bei uns genau richtig!

Wir freuen uns immer über neue Gesichter. Wir veranstalten immer wieder Treffen, bei denen wir uns vorstellen. Sie sind der beste Ort für erste Informationen und deine Fragen.

Die Termine kündigen wir rechtzeitig auf dieser Website an.

Oder schreib uns direkt unter {{< email "kontakt@ask-freiburg.net" >}}.


## Spenden

Seit Jahren arbeiten wir alle auf ehrenamtlicher Basis. Unser Verein hat laufende Kosten für Material, Vortragsveranstaltungen, Weiterbildungen und Fahrten unserer ehrenamtlichen Mitarbeiter:innen und wir sind deshalb dankbar für jede Spende!

{{< image src="donation-icon.png" alt="Spenden-Symbol" width=200 width-mobile=150 float="right" margin="" no-resize="" >}}

Spenden an uns sind steuerlich absetzbar, da ask! e.V. ein gemeinnütziger Verein ist. Das Finanzamt erkennt einen Kontoauszug als Spendenbeleg bis zu einer Summe von 200 € an. Für darüber hinaus gehende Spenden stellen wir gern eine Spendenbescheinigung aus.

**Kontoverbindung:**

> Empfänger: ask! Außerstationäre Krisenbegleitung e.V.\
> IBAN: DE88 4306 0967 7925 7895 00\
> Bank: GLS Bank


## Newsletter

Um sich unverbindlich über aktuelle Entwicklungen bei ask! zu informieren, abboniere am besten unseren [Newsletter](/newsletter-info) (ca. 4-6 Mails pro Jahr).


## Vereinsmitglied oder -förderer werden

Um Mitglied bei ask! e.V. zu werden oder den Verein regelmäßig durch einen Förderbeitrag zu unterstützen, findest du hier die nötigen Formulare:

* {{< filelink src="Mitgliedsantrag-form.pdf" text="Unterstützungserklärung/Aufnahmeantrag" >}}
* {{< filelink src="2019-08-02_Satzung-ask.pdf" text="Satzung des Vereins ask! e.V." >}}
