---
title: Netzwerkgespräche
background: /assets/images/bg/spaziergang.jpg
menu:
  main:
    identifier: netzwerkgespraeche
    weight: 3
slug: netzwerkgespraeche
---
{{< image src="offener-dialog-icon.png" alt="Offener-Dialog-Symbol" width=200 width-mobile=150 float="right" >}}

Der Offene Dialog ist ein systemischer Ansatz zur Unterstützung und Begleitung von Menschen in psychischen Krisen. Er wurde in Skandinavien entwickelt und ist gleichzeitig Philosophie und Haltung: An sogenannten Netzwerkgesprächen  können sich alle beteiligen, die zum sozialen und professionellen Netzwerk eines sich in der Krise befindenden Menschen gehören -  falls dies von ihm gewünscht wird. Die Moderation dieser Gespräche erfolgt nach den Prinzipien des Offenen Dialogs, d.h. völlige Transparenz zwischen den Beteiligten, Annahme der Unterschiedlichkeit von Gefühlen und Denken, Aushalten von Unsicherheit, Zurückhaltung bei Medikamenten, Begleitung möglichst Zuhause.

Netzwerkgespräche können bei verschiedensten Krisensituationen hilfreich sein. Am besten wurde der Ansatz des Offenen Dialogs in Bezug auf die Behandlung von Psychosen erforscht und erzielte darin gute Ergebnisse. Ein Netzwerkgespräch kann entweder von einer Person, die in einer Krisensituation ist, oder von einer Person aus deren Netzwerk initiiert werden. Im Netzwerkgespräch können gemeinsam weitere Schritte besprochen werden, wie die Organisation einer Krisenbegleitung zuhause (zu der regelmäßige Gespräche oder Spaziergänge gehören könnten) oder die Inanspruchnahme weiterer ambulanter Dienste oder anderweitiger fachlicher Begleitung (in Freiburg z.B. Freizeitangebote der Freiburger Hilfsgemeinschaft oder des Sozialpsychiatrischen Dienstes).

Derzeit gibt es im Raum Freiburg noch keine verbindlichen Angebote für Netzwerkgespräche. Aber über ask! gibt es die Möglichkeit, Personen, die in der Moderation des Offenen Dialogs ausgebildet sind, zu kontaktieren und anzufragen unter {{< email "nwg-anfrage@ask-freiburg.net" >}}
