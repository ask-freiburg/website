---
date: 2023-06-25T15:41:15.498Z
title: "2023-03: Regio Freiburg Krisendienst: es tut sich was,
  Krisenbegleitungsschulung, Fortbildung OD: start erst im Oktober"
---
Hallo liebe ask!-Interessierte,

Hier kommt der dritte Newsletter des Jahres 2023, diesmal mit diesen Themen:

* ask! im GPV für einen Regio Krisendienst Freiburg: es tut sich was!
* Macht mit beim Krisenteam von ask! ! - Krisenbegleitungs-Schulung geplant
* Fortbildung Offener Dialog mit Volkmar Aderhold: ab OKTOBER 2023 in Freiburg (Start wurde verschoben!)
* Termine

### ask! im GPV für einen Regio Krisendienst Freiburg: es tut sich was!

Beim Projekt "Freiburger Krisendienst" geht es gerade in großen Schritten voran! Der im Januar-Newsletter erwähnte Zusammenschluss der drei GPV-Mitglieder

* ask! e.V.
* Freiburger Hilfsgemeinschaft e.V. (FHG)
* Start e.V.

hat sich inzwischen koordiniert und es wurden nun schon wichtige Schritte unternommen, um einen Krisendienst für die Region Freiburg auf den Weg zu bringen:

Wir (die drei Vereine) haben gemeinsam die gemeinnützige Gesellschaft (mbH) "Regio Krisendienst" gegründet (die Anerkennung der Gemeinnützigkeit wird derzeit noch geprüft). Sie bildet das organisatorsiche Gerüst für die weitere Zusammenarbeit und wird für den Betrieb des Krisendienstes zuständig sein.

Damit die "Regio Krisendienst" den Krisendienst ins Leben rufen und betreiben kann, soll sie nun zunächst Mitglied im Paritätischen werden. Dann wird sie einen Antrag auf finanzielle Anschubförderung des Krisendienstes bei Aktion Mensch stellen.

Derzeit kümmert sich noch die Gesellschafterversammlung, bestehend aus Mitgliedern der drei Gründungs-Gesellschafter:innen (also Gründungs-Vereine), um all diese Dinge, ab Herbst/Winter hoffen wir eine Leitungsstelle für die Geschäftsführung der "Regio Krisendienst" besetzen zu können. Durch die Rechtsform der gGmbH ist die Möglichkeit offen gehalten, dass in Zukunft auch noch weitere Akteure als Gesellschafter:innen den Krisendienst mitgestalten.

Der Krisendienst soll mit einem kleinen Angebot beginnen und dann stufenweise immer weiter ausgebaut werden: zunächst soll die telefonische Erreichbarkeit an Wochenenden und dann an immer mehr Abenden, Nächten und Tagen angeboten werden. Ein zentrales Element sollen aufsuchende multiprofessionelle Krisenteams und das Angebot von Netzwerkgesprächen mit "Offenem Dialog" sein.

In den vergangen Monaten hat sich also einiges bewegt und wenn es so weiter geht, ist die erste Stufe des Krisendienstes schon bald Realität. Wir halten euch auf dem laufenden!

### Macht mit beim Krisenteam von ask! ! - Krisenbegleitungs-Schulung geplant

Unabhängig vom Projekt Krisendienst wollen wir von ask! auch eine Krisenbegleitung anbieten. Die Grundidee ist, dass es in einer Krise hilfreich sein kann, gesehen und verstanden zu werden, dass es hilft, wenn jemand in der Situation "da sein" kann. Das muss auch gar kein langjährig ausgebildeter Mensch sein, die wichtigste Expertise ist oft die eigene Lebens- und vielleicht auch Krisen-Erfahrung. Wir wollen uns als Krisenteam organisieren um genau das zu tun: für Menschen in Krisen da sein und Begleitung anbieten.\
Dabei sollen Fachkräfte, als Genesungsbegleiter ausgebildete Psychiatrieerfahrene, Angehörige und engagierte Bürgerinnen und Bürger zusammen die bedürfnisorientierte Begleitung von betroffenen Menschen in psychiatrischen Krisen in deren vertrautem Wohnumfeld oder in einem neutralen Umfeld anbieten.

Wenn du im Krisenteam bei ask! mitmachen möchtest, dann melde dich unter krisenbegleitung@ask-freiburg.net ! Wir treffen uns das nächste Mal am Mo, 03.07. um 19:00 Uhr.

Außerdem organisieren wir für Interessierte eine Grundlagenschulung zum Thema "Krisenbegleitung" in 8 Modulen. Sie wird im Herbst diesen Jahres starten und im Programm der Volkshochschule ausgeschrieben sein.

Du kannst dich aber auch bereits jetzt bei uns melden, wenn du Interesse an der Schulung hast.

Geplante Inhalte der Schulung "Krisenbegleitung":

* Einführung in das Konzept + Eigenmotivation
* Bedürfnisangepasste Begleitung und Offener Dialog
* Leitfaden für ambulante Krisenbegleitung des BPE
* Recovery und Empowerment
* Gewaltfreie Kommunikation
* Deeskalationsstrategien
* Gemeindepsychiatrisches Netzwerk der Region
* Praktische Umsetzung des Projekts

### Fortbildung Offener Dialog mit Volkmar Aderhold ab OKTOBER 2023 in Freiburg

Der Start der Fortbildung "Offener Dialog" wurde nun auf Oktober verschoben, da derzeit die Mindest-Teilnehmerzahl noch nicht erreicht ist. Mit aktuell 20 Anmeldungen fehlen noch mindestens 6 Teilnehmer:innen, damit die Fortbildung stattfinden kann (die Teilnehmerzahl ist allerdings auch auf 30 begrenzt). Wer noch Menschen kennt, für die diese Fortbildung interessant sein könnte, bitte folgenden Link weitergeben:

<https://www.ask-freiburg.net/fortbildung-offener-dialog/>

### Termine

* Mo, 26.06. 18:30: Trialog, in den Räumen der FHG/Club 55, Schwarzwaldstraße 9 im 2. OG.
* Mo, 03.07. 19:00: Treffen vom ask! Krisenbegleitungsteam für Krisenbegleiter:innen. Bei Interesse bitte an krisenbegleitung@ask-freiburg.net schreiben
* Fr, 22.09. : Fachtag des Fachausschuss Psychopharmaka der DGSP e.V. in Freiburg, Thema: Psychosebegleitung und Neuroleptika <https://www.dgsp-ev.de/tagungen/aktuelle-tagungen-der-dgsp/fachtag-psychopharmaka.html>