---
date: 2025-03-01T12:47:11.252Z
title: "2025-01: Neuigkeiten vom Krisendienst, Termine für Fortbildung,
  REGIO-Tagung in Freiburg"
---
Hallo liebe ask!-Interessierte,\
\
Das Jahr ist schon nicht mehr ganz ganz jung und der letzte Newsletter schon ein Weilchen her. Aber hier kommt er, unser erster Newsletter in diesem Jahr. Diesmal mit folgenden Themen:

* Es geht voran: die Vorbereitungen vom Regio Krisendienst laufen
* Fortbildung Offener Dialog 2025: Termine stehen fest
* Tagung REGIO 2025 in Freiburg
* Termine

### Es geht voran: die Vorbereitungen vom Regio Krisendienst laufen

Ein Vierteljahr ist vergangen seit wir euch zuletzt angefragt haben, ob es Interesse an einer Mitarbeit im Krisendienst gibt -- und das Interesse fiel tatsächlich überwältigend groß aus! Nach 3 stattgefundenen Infoabenden haben wir nun einen Pool von über 60 Menschen, die gerne mitmachen würden – und wir müssen uns überlegen, wie ein „geordnetes“ Auf- und Ausbauen dieses Teams gehen könnte. Neben Ida Wehinger, die als erste hauptamtliche Koordinierende Mitte Oktober gestartet ist und von Uta Hempelmann und Friedhilde Rissmann-Schleip begleitet wurde ist dieses Leitungs-Team nun mit einem jeweiligen Stellenanteil von 50% (Ida), 37% (Uta) und 13% (Friedhilde) zusammengestellt und arbeitet an den organisatorischen Vorbereitungen für den konkreten Start ab 1.4.2025!\
\
D.h. am Freitagabend den 4.4.2025 wird zum allerersten Mal unser Telefon besetzt sein mit 2 möglichen offenen Leitungen! Vorher aber eben auch nicht, und deswegen geben wir die Nummer auch noch nicht raus und bitten dafür um Verständnis, denn es wäre nicht gut, wenn jemand anrufen würde und niemanden erreicht zu den angegebenen Zeiten!\
\
Bis dahin laufen die weiteren Vorbereitungen auf Hochtouren:\
\
Wir haben ein sehr vielfältiges trialogisches Team mit ganz unterschiedlichen und breit gefächerten Qualitäten, die wir in kleineren 5-Gruppengesprächen auch schon ein bisschen näher kennenlernen durften.\
Am letzten Freitag hatten wir auch unser erstes 3-stündiges Fortbildungsangebot mit Herrn Ellensohn, dem Leiter der Telefonseelsorge in Freiburg, der diese Fortbildung auch schon zu Corona-Zeiten online für uns angeboten hatte.\
Im März werden Ida und Uta und Friedhilde ein Wochenende anbieten, in dem es um weitere Selbstreflexion und gute Eigeneinschätzung und die eigene bzw. gemeinsame Haltung gehen wird sowie um Informationen zu den Leistungsanbietern in Freiburg und der Regio, um einen Überblick zu bekommen, wohin man Hilfesuchende evtl. auch hin verweisen könnte.\
Im April wird Ralf Bohnert, der langjährige Leiter des mittelfränkischen Krisendienstes zum dritten Mal (innerhalb von 6 Jahren, auch letztes Jahr war er bereits im April für uns hier) zu uns kommen, um uns mit seinen Erfahrungen fortzubilden.\
In den kommenden Tagen werden wir mal einen ersten „Dienstplan“ erstellen und dann auch verbindliche Honorarverträge abschliessen wollen. Die Vergütung der Arbeitsstunden liegt bei 18€/Stunde. Wir wünschen uns die Bereitschaft mindestens 1x monatlich einen Dienst (mit insgesamt 5 Stunden) zu übernehmen, doch es scheint genügend Bereitwillige auch zu mehr Arbeitsstunden zu geben.\
Das bedeutet auch, dass wir wahrscheinlich zügiger als gedacht unsere Angebotszeiten ausdehnen können und möglicherweise auch früher unsere Mobilen Teams bereitstellen werden können.\
\
Ebenso würden wir gern das Angebot für Netzwerkgespräche bereitstellen können, aber auch hierfür braucht es erstmal eine Finanzierungsgrundlage, die wir noch nicht haben.\
Daher müssen wir uns auch beständig um eine Erweiterung der Finanzierung bemühen, damit diese gewünschte inhaltliche und zeitliche Erweiterung auch finanziell unterfüttert ist.\
\
Ebenso haben wir ja mit Prof. Fabian Frank von der Ev. Hochschule Freiburg einen langjährigen Unterstützer unseres Anliegens an unserer Seite, der uns gerne begleitforschen würde zusammen mit einer Kollegin, aber auch hierfür braucht es trotz allem Entgegenkommen seinerseits noch zusätzliche Gelder, die wir akquirieren müssen.\
\
In den letzten 3 Monaten waren wir vor allem mit Organisation von notwendigen Versicherungen, Gehaltskonten, erstellen von Mailkonten, Website, Flyer und Werbepostkarten, Telefonanlage, Beschaffung von Infrastruktur (PC, Handys, Möbel, Datenbank) und auch schon der ein oder anderen Wohnungsbesichtigung beschäftigt (gerne weitergeben, wenn jemand was weiß :1 Büroraum und 1 Gruppenraum plus Teeküche!)\
\
Und wir haben begonnen zu netzwerken und uns bereits mit dem AKL, dem SPDI und der Polizei getroffen, weitere mögliche Kooperationsgespräche sind bereits anvisiert.\
\
Ihr seht: es gibt viel zu tun und es läuft auch schon viel um den Regio Krisendienst ins Rollen zu bringen!

### Fortbildung Offener Dialog 2025: Termine stehen fest

Wir wollen erreichen, dass über den Krisendienst langfristig auch ein Angebot für Netzwerkgespräche im Offenen Dialog geschaffen bzw. angebunden wird. Dazu braucht es natürlich auch Menschen, die solche Netzwerkgespräche moderieren können. Deshalb bieten wir die systemische Fortbildung: "Offener Dialog - Sozialraumorientierte Netzwerkarbeit" auch 2025 nochmal in Freiburg an und schaffen somit die Möglichkeit sich direkt hier in der Region unter Leitung von Dr. med. Volkmar Aderhold, FA für Psychiatrie und Psychotherapie, fortbilden zu lassen! Die Weiterbildung wird ausgerichtet durch ask! e.V. Freiburg und startet im Oktober 2025. Mehr Informationen, die genauen Termine und die Möglichkeit zur Anmeldung gibt es unter\
\
<https://www.ask-freiburg.net/fortbildung-offener-dialog/>

### Tagung REGIO 2025 in Freiburg

Die REGIO-Tagung findet dieses Jahr in Freiburg statt! Unter dem Motto "BTHG – Anspruch und Wirklichkeit Was hat es gebracht?" finden Anfang April zahlreiche Vorträge und Workshops statt:\
\
**Termin:** 4. April 2025, 9:30 – 17:00 Uhr\
**Ort:** Katholische Akademie Freiburg, Wintererstraße 1, 79104 Freiburg\
\
Weitere Informationen, Programm-Flyer und Anmeldung:\
\
<https://www.gemeindepsychiatrie-bw.de/2025/01/regio-2025/>

### Termine

* Mo, 31.03. um 18:30 Uhr: Trialog, in den Räumen der FHG/Club 55, Schwarzwaldstraße 9 im 2. OG.
* Fr, 4. April 2025: REGIO-Tagung in Freiburg (siehe <https://www.gemeindepsychiatrie-bw.de/2025/01/regio-2025/> )