---
date: 2021-09-29T15:55:38.411Z
title: "2021-04: Welttag seelische Gesundheit am 10. Oktober, GPV-Fachtag 2021"
---
Hallo liebe ask!-Interessierte,

hier kommt der vierte Newsletter 2021, diesmal mit diesen Themen:

* Welttag seelische Gesundheit am 10. Oktober
* Erinnerung: GPV-Freiburg - Fachtag 2021 zum Thema Recovery am Mo, 04. Oktober
* Radiosendung ‘VielFalter’

## Welttag seelische Gesundheit am 10. Oktober

Anlässlich des Welttags der seelischen Gesundheit am 10. Oktober hat die Psychiatrie-Koordination vom Amt für Soziales und Senioren in Freiburg ein kleines Programm mit dem Thema "(K)eine_r ist allein krank - Psychische Gesundheit in der Familie" zusammen gestellt und gibt Einrichtungen der gemeindepsychiatrischen Versorgung Gelegenheit sich vorzustellen.

Das Programm ist als [Datei in der Anlage](https://www.freiburg.de/pb/,Lde/530411.html).

Poster und Flyer können auch in Papierform angefordert werden bei:

Kontakt: Christine Kubbutat (Freiburg Amt für Soziales und Senioren, Psychiatrie-Koordination)

## Erinnerung: (GPV) Freiburg - Fachtag 2021 am Mo, 04. Oktober

Erinnerung: Am Mo, 04. Oktober findet der Fachtag des Gemeindepsychiatrischen Verbunds Freiburg (GPV) unter dem Titel „Recovery – (K)ein Patentrezept?!“ statt (siehe letzter Newsletter).

## Radiosendung ‘VielFalter’

Die Radiosendung ‘VielFalter’ von Mirko auf Radio Dreyeckland ist inzwischen jeweils am ersten Montag des Monats von 20 bis 21 Uhr on  Air (Wiederholung tags drauf um 13 Uhr). Die Septembersendung 2021 findet Ihr unter <https://vielfalter.podspot.de/post/sendung-vom-6-september-2021-mit-norman-wolf-zum-thema-mobbing/> zum Herunterladen und Anhören.

Die Juli-Sendung 2021 war im letzten Newsletter leider falsch verlinkt. Ihr findet sie unter
<https://vielfalter.podspot.de/post/sendung-vom-5-juli-2021-mit-karina-korecki-und-heike-petereit-zipfel/>