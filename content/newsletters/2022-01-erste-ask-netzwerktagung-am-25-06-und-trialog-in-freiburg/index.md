---
date: 2022-03-03T13:17:15.010Z
title: "2022-01: erste ask! Netzwerktagung am 25.06. und Trialog in Freiburg"
---
Hallo liebe ask!-Interessierte,

Hier kommt der erste Newsletter des Jahres 2022, diesmal mit diesen Themen:

* ask! Netzwerktagung "Krisenbegleitung und Offener Dialog": am Sa, 25.06. im Bürgerhaus Zähringen
* Trialog Freiburg
* Hometreatment-Tagung in Bremen am 05./06. Mai
* Radiosendung ‘VielFalter’

### ask! Netzwerktagung "Krisenbegleitung und Offener Dialog"

Am Sa, 25. Juni 2022 findet in Freiburg die erste ask! Netzwerktagung statt. Die Themenschwerpunkte sind Krisenbegleitung und Offener Dialog – welches auch die beiden Hauptthemen sind, für die sich die Freiburger Initiative ask! seit nunmehr 10 Jahren einsetzt.

Mit Vorträgen und Workshops – unter anderem von und mit Volkmar Aderhold, Ralf Bohnert und Aktiven des Leipziger [Offener Dialog e.V.](https://offenerdialog-ev.de/ "https\://offenerdialog-ev.de/") – wollen wir einen Raum für neue Impulse, Austausch und organisatorische Vernetzung zu diesen Themen im deutschsprachigen Raum schaffen. Die trialogisch ausgerichtete Tagung richtet sich sowohl an politisch Aktive als auch an interessierte Menschen mit und ohne Krisenerfahrung bzw. mit und ohne psychiatrische/therapeutische/pflegerische Ausbildung.

Weitere Infos, das vollständige Programm und die Anmeldung gibt es auf unserer Website unter

[https://www.ask-freiburg.net/tagung/](https://www.ask-freiburg.net/tagung/ "https\://www.ask-freiburg.net/tagung/")

Hinweis zur Anmeldung: Derzeit läuft noch ein Förderantrag für die Tagung. Wenn dieser erfolgreich ist, kann der Teilnahmebetrag von den bisher veranschlagten 90 EUR (bzw. 60 EUR ermäßigt) nochmal deutlich gesenkt werden. Es ist aber schon jetzt eine unverbindliche Anmeldung möglich. Sobald der Teilnahmebetrag endgültig festgelegt ist (im Laufe des März) werden alle bis dahin angemeldeten Menschen kontaktiert und eine verbindliche Anmeldung angeboten. Dies gilt auch für alle, die sich bereits schon mit Tagungsbeitrag angemeldet haben. Wenn es dann noch Restplätze gibt, werden diese danach zur verbindlichen Anmeldung auf der Website freigegeben.

### Trialog Freiburg

Der Trialog in Freiburg findet wieder statt: Trialog ist keine therapeutische Methode, sondern eine neue Form der Beteiligungskultur. Im Trialog gehen Psychiatrieerfahrene, Angehörige und in der Psychiatrie Tätige als Experten in eigener Sache aufeinander zu, um voneinander zu lernen.

Der Freiburger Trialog trifft sich in der Regel jeden letzten Montag im Monat um 18.30 Uhr in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.

Nächster Termin: Mo, 28. März um 18:30 Uhr

Es gilt das Hygienekonzept der Freiburger Hilfsgemeinschaft und damit unter anderem 3G und FFP2-Maskenpflicht (siehe [https://www.fhgev.de/](https://www.fhgev.de/ "https\://www.fhgev.de/") )

Weitere Infos: [https://www.fhgev.de/gruppen/trialog-in-freiburg.html](https://www.fhgev.de/gruppen/trialog-in-freiburg.html "https\://www.fhgev.de/gruppen/trialog-in-freiburg.html")

### Hometreatment-Tagung in Bremen am 05./06. Mai

Folgende Vorankündigung leiten wir gerne weiter:

Am 5./6. Mai 2022 findet die diesjährige Hometreatment-Tagung in Bremen statt. Diese Tagung gibt es seit 12 Jahren und spricht alle die an, die sich mit der Behandlung von Krisen zu Hause befassen, gleich welcher therapeutischen Auffassung sie dabei sind. Wenn Ihr oder Euer Kreis daran Interesse habt/hat, könnt Ihr Euch an Nils Greve [greve@psychiatrie.de](mailto:greve@psychiatrie.de "mailto\:greve@psychiatrie.de") wenden. Er verwaltet den Verteiler für die Einladungen.

### Radiosendung ‘VielFalter’

Die Radiosendung ‘VielFalter’ von Mirko auf Radio Dreyeckland vom 7.2.2022 mit Matthias Heißler ist online zu finden unter [https://rdl.de/sites/default/files/audio/2022/02/20220207-vielfalterma-a16440.mp3](https://rdl.de/sites/default/files/audio/2022/02/20220207-vielfalterma-a16440.mp3 "https\://rdl.de/sites/default/files/audio/2022/02/20220207-vielfalterma-a16440.mp3") . Nur das Gespräch ohne die Musik und eine kurze Beschreibung ist verfügbar unter: [https://www.freie-radios.net/113889](https://www.freie-radios.net/113889 "https\://www.freie-radios.net/113889")

Weitere Sendungen und Beiträge sind derzeit noch unter [http://vielfalter.podspot.de](http://vielfalter.podspot.de "http\://vielfalter.podspot.de") zu finden.