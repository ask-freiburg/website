---
date: 2022-09-15T17:18:26.570Z
title: "2022-06: ask! Treffen für Neue, der besondere Film, Online-Petition und
  -Lyrik"
---
Hallo liebe ask!-Interessierte,

Hier kommt der sechste Newsletter des Jahres 2022, diesmal mit diesen Themen:

* ask! Treffen für Neue am Mi, 09.11. um 19:00 Uhr
* Film "Someone Beside You" diesen So, 18.09. um 19:30 Uhr
* Online-Petition für einen Kulturkiosk am Stühlinger Kirchplatz
* Online-Lyrik-Veranstaltung vom RCS am Di, 11.10. um 17:00 Uhr
* 10.10. - 24.10.: Welttag seelische Gesundheit 2022: Einsam? – Damit bist Du nicht allein …!

### ask! Treffen für Neue am Mi, 09. November um 19:00 Uhr

Treffen für Neue/Interessierte bei ask!

Am Mi, 09. November um 19:00 Uhr
in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.

Das Treffen richtet sich an Menschen, die erst seit kurzem oder noch gar nicht bei ask! dabei sind. Super, um mehr über ask! zu erfahren, Fragen beantwortet zu bekommen und rauszufinden, wie man sich bei ask! einbringen kann.

Interessierst du dich dafür Menschen in Krisensituationen zu unterstützen? Hast du selbst Erfahrungen (im Umgang) mit seelischen Krisen? Dann komm gerne unverbindlich zu diesem offenen Treffen!

Generell ist das Treffen interessant für alle Menschen (mit und ohne Psychiatrie-Erfahrung sowie Angehörige)

* die unsere Arbeit unterstützen möchten
* die KrisenbegleiterInnen werden möchten
* die sich ausführlicher über die Arbeit von ask! informieren möchten

### Film "Someone Beside You" diesen So, 18.09. um 19:30 Uhr

In der kleinen Filmreihe des "besonderen Films" werden im Rahmen des Sonntagskinos folgende Filme präsentiert:

So, 18.09.2022 "Someone beside you" - Edgar Hagen (2006)
(Infos siehe unten)

und - zum Anlass der 3. Jährung ihres Todestages - am
So, 09.10.2022 "Dorothea Buck - vom Wahn zum Sinn" - Edgar Hagen (1998)

beide Aufführungen finden um 19:30 Uhr im Gemeinschaftsraum der schwereLos GmbH in der Arne-Torgersen-Str. 7 statt.

Infos zum Film: Someone Beside You
98 Min, Color, Dolby Digital, 35mm, 2006, OV CH-German/English (deutsch untertitelt)

<https://www.someonebesideyou.com>

Zusammen mit einigen Psychiatern und deren Klienten bricht der Film zu einem dokumentarischen Roadmovie durch die Schweiz, Europa und die USA auf. In Wohnmobilen durchreisen sie die Abgründe der Psyche und gehen existenziellen Frage nach: Was ist der menschliche Geist? Wie verhält er sich in psychotischen Extremsituationen? In den USA begegnet Edgar Hagen dem buddhistischen Mönch und Psychiater Edward Podvoll. der nur noch wenige Monate zu leben hat. Seine Vision, dass Mut und Freundschaft die Kraft zur Heilung von Psychosen haben, ist sein inspirierendes Vermächtnis. Im Dialog zwischen westlicher Psychologie und östlicher Spiritualität eröffnet sich, dass auch aus größter Verwirrung heraus geistige Klarheit möglich wird.

### Online-Petition für einen Kulturkiosk am Stühlinger Kirchplatz

Folgenden Aufruf geben wir gerne weiter:

Der geplante KulturKiosk braucht eine breite Zustimmung um der Freiburger Stadtverwaltung gegenüber ein deutliches Zeichen zu setzen und die Umsetzung zu erwirken und zu beschleunigen (ein großer Förderantrag bei einer Stiftung ist bereits in Bearbeitung)!

bitte unterstützt unser Vorhaben mit Eurer digitalen Unterschrift (anonym oder mit Statement)!

Hier geht’s zur Petition: <https://www.change.org/p/kulturkiosk-am-st%C3%BChlinger-kirchplatz-f%C3%BCr-begegnung-statt-ausgrenzung>

Wer sich die neuen Entwürfe des KIT (Karlsruher Institut für Technologie) anschauen möchte und sich für weitere Infos zu unserem KulturKiosk interessiert, hier klicken: <https://www.schwere-s-los.de/kulturkiosk/>

Herzliche Grüße und noch herzlicheren Dank für die Unterstützung,

Michael & das gesamte Team von Schwere(s)Los! e.V.

### Online-Lyrik-Veranstaltung am Di, 11.10. um 17:00 Uhr

Diese Einladung vom RecoveryCollege Südbaden leiten wir gerne weiter:

Hoffnung: Es ist nicht immer leicht, dass es besser wird...

Uns ist es gelungen drei psychiatrieerfahrene Dichterinnen und Dichter zu finden, die bereit sind uns ihre Gedichte nahe zu bringen. So unterschiedlich die Menschen sind, so unterschiedlich sind deren Gedichte. Sie dürfen sich auf die Lebenswelten von Menschen freuen, die schon einen schweren Gang gegangen sind, denen das Schreiben ein unverzichtbarer Gefährte geworden ist und die so auf besondere Weise zu ihrem Recoveryweg gefunden haben.

Es lesen:

* Heike Bader, Freudenstadt
* Berthold Bausch, Freiburg
* Annette Wilhelm, Bühl

Diese Online-Lyrik-Veranstaltung findet am Dienstag, 11.10.2022 von 17:00-18:30 Uhr statt.

Die Zoom-Zugangsdaten lauten:

\[nicht online verfügbar, bitte bei Interesse anfragen]

Wir freuen uns sehr, wenn Sie an der Veranstaltung teilnehmen!

Ihr RCS-Team

### Welttag seelische Gesundheit 2022: Einsam? – Damit bist Du nicht allein …!

Vom 10.10. bis 24.10. läuft das Programm zum Welttag seelische Gesundheit 2022 in Freiburg unter dem Motto: Einsam? – Damit bist Du nicht allein …!

Das vollständige Programm ist zu finden unter <https://www.freiburg.de/pb/530411.html>

### Termine

* So, 18.09. 19:30: Film "Someone beside you" - Edgar Hagen (2006) im Gemeinschaftsraum schwereLos (Arne-Torgersen-Str. 7)
* Mo, 26.09. 18:30: Trialog in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.
* So, 09.10. 19:30: Film "Dorothea Buck - vom Wahn zum Sinn" - Edgar Hagen (1998) im Gemeinschaftsraum schwereLos (Arne-Torgersen-Str. 7)
* Di, 11.10. 17:00: Online-Lyrik-Veranstaltung vom RecoveryCollege Südbaden (RCS)
* 10.10. - 24.10.: Programm zum Welttag seelische Gesundheit 2022: Einsam? – Damit bist Du nicht allein …! siehe <https://www.freiburg.de/pb/530411.html>
* Mi, 09.11. 19:00: ask! Treffen für Neue in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.