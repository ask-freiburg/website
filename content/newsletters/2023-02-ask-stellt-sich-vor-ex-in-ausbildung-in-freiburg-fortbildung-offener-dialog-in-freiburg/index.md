---
date: 2023-02-13T14:27:49.370Z
title: "2023-02: ask! stellt sich vor, EX-IN-Ausbildung in Freiburg, Fortbildung
  Offener Dialog in Freiburg"
---
Hallo liebe ask!-Interessierte,

Hier kommt der zweite Newsletter des Jahres 2023, diesmal mit diesen Themen:

* ask! stellt sich vor: Treffen für Interessierte am 07. März um 18:30 Uhr
* Neuer EX-IN Kurs in Freiburg: Bewerbungsverfahren läuft
* Fortbildung Offener Dialog mit Volkmar Aderhold ab Mai 2023 in Freiburg
* Termine

### ask! stellt sich vor: Treffen für Interessierte am 07. März um 18:30 Uhr

Wir laden herzlich ein zum Treffen für Neue/Interessierte bei ask!

**Wann:** Am Di, 07. März um 18:00 Uhr\
**Wo:** in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.

Das Treffen richtet sich an Menschen, die erst seit kurzem oder noch gar nicht bei ask! dabei sind. Super, um mehr über ask! zu erfahren, Fragen beantwortet zu bekommen und rauszufinden, wie man sich bei ask! einbringen kann.

### Neuer EX-IN Kurs in Freiburg: Bewerbungsverfahren läuft

Gerne leiten wir folgende Info von EX-IN Südbaden weiter:

Sehr geehrte Damen und Herren,

ich möchte Sie darüber informieren, dass das Bewerbungsverfahren für den EX-IN Kurs 2023/2024 von EX-IN begonnen hat.

EX-IN ist eine Qualifizierungsmaßnahme für psychiatrie-/-krisenerfahrene Menschen, die zum /zur EX-IN Genesungsbegleiter/-in fortgebildet werden, um danach als (bezahlte) Fachkräfte im psychiatrischen System zu arbeiten zu können.

Der Kurs startet am **5. Mai 2023** und wird nach 12 Wochenend-Modulen im April 2024 abgeschlossen sein. Er wird in Freiburg stattfinden.

Die Bewerbungsunterlagen können bei mir angefordert werden und sollten bis zum

**17. Februar** bei uns eingehen.

Weitere Infos zum Kurs finden Sie im anhängenden Infoblatt.

Wir möchten Sie bitten, diese Information an Interessierte weiterzuleiten.

Vielen Dank und\
mit freundlichen Grüßen

das Team von EX-IN Südbaden

### Fortbildung Offener Dialog mit Volkmar Aderhold ab Mai 2023 in Freiburg

Es gibt noch freie Plätze bei der Fortbildung zum Offenen Dialog in Freiburg. Inzwischen ist auch das Curriculum auf der Website abrufbar:

<https://www.ask-freiburg.net/fortbildung-offener-dialog/>

### Termine

* Mo, 27.02. 18:30: Trialog, in den Räumen der FHG/Club 55, Schwarzwaldstraße 9 im 2. OG.
* Di, 07.03. 18:30: ask! stellt sich vor: Treffen für Neue/Interessierte
* Mi, 05.04. 15:00: Online-Veranstaltung: Zwischen Selbstbestimmung und Fürsorge: Wie gelingt „freies Handeln” im Kontext psychiatrischer Behandlung