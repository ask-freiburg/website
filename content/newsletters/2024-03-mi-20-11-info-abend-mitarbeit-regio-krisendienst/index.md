---
date: 2024-11-17T22:20:22.689Z
title: "2024-03: Mi, 20.11.: Info-Abend Mitarbeit Regio-Krisendienst"
---
Hallo liebe ask!-Interessierte,

Hier kommt der dritte Newsletter des Jahres 2024. Diesmal kurz und übersichtlich mit folgenden Themen:

* Mi, 20.11.: Info-Abend Mitarbeit Regio-Krisendienst
* Fortbildung Offener Dialog -- nach der Fortbildung ist vor der Fortbildung
* Termine


### Mi, 20.11.: Info-Abend Mitarbeit Regio-Krisendienst

Wir freuen uns sehr, dass die Finanzierung des Krisendienstes nun nach langer Geduldsprobe für die ersten Jahre gesichert ist.
Somit kann nun alles ins Laufen kommen!

Wir planen Anfang 2025 mit dem Telefondienst an den Wochenenden zu starten.

Dafür brauchen wir motivierte Mitarbeiter*innen! Wenn Sie an einer freiberuflichen, bzw. ehrenamtlichen Mitarbeit beim Krisendienst als Honorarkraft interessiert sind und sich einen Telefondienst von 5 Stunden, inklusive Vor- und Nachbereitungszeit vorstellen können, dann würden wir Sie gerne an unserem Info-Abend begrüßen:

> Mittwoch, 20. November 2024
> 17:00 – 19:00 Uhr
> in den Räumen der FHG e.V., Schwarzwaldstraße 9, 79117 Freiburg (2. OG)

Wir möchten Ihnen an diesem Abend ein kurzes Update zum Krisendienst geben, zu den Rahmenbedingungen und Anforderungen an Mitarbeiter*innen, sowie uns (nochmals) kennenlernen.

Wir bitten um eine formlose Anmeldung an kontakt@regio-krisendienst.de wenn Sie an dem Info-Abend teilnehmen möchten.

Wir suchen außerdem nach einer zweiten Teilzeitstelle für die Koordinationsleitung des Krisendienstes (siehe  Stellenausschreibung im Anhang).

Weitere Informationen:
<https://www.ask-freiburg.net/posts/der-regio-krisendienst-kann-starten-mi-20.11.-info-abend-mitarbeit/>


### Fortbildung Offener Dialog -- nach der Fortbildung ist vor der Fortbildung

Im Oktober fand das Abschließende Modul der im Oktober 2023 gestarteten systemischen Fortbildung: "Offener Dialog - Sozialraumorientierte Netzwerkarbeit" statt. Gut 20 Menschen aus dem deutschsprachigen Raum wurden dort von insgesamt 9 Referent:innen erfolgreich fortgebildet für die Moderation von Netwerkgesprächen im Offenen Dialog.

Wir bedanken uns ganz Herzlich für die finanzielle Förderung der Fortbildung durch Aktion Mensch und durch die Waisenhausstiftung Freiburg!


Auch 2025 wird die Fortbildung noch einmal angeboten werden. Die Weiterbildung wird ausgerichtet durch ask! e.V. Freiburg und startet im Oktober 2025. Mehr Informationen und die Möglichkeit zur Anmeldung gibt es unter

<https://www.ask-freiburg.net/fortbildung-offener-dialog/>



### Termine

* Mo, 25.11. um 18:30 Uhr: Trialog, in den Räumen der FHG/Club 55, Schwarzwaldstraße 9 im 2. OG.
* Fr, 4. April 2025: REGIO-Tagung in Freiburg (siehe https://www.gemeindepsychiatrie-bw.de/2024/04/save-the-date-cloned-regio-2024/ )