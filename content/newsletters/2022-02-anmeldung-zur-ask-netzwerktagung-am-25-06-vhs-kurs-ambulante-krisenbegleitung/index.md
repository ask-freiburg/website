---
date: 2022-03-26T16:48:30.586Z
title: "2022-02: Anmeldung zur ask! Netzwerktagung am 25.06., VHS-Kurs ambulante
  Krisenbegleitung"
---
Hallo liebe ask!-Interessierte,

Hier kommt der zweite Newsletter des Jahres 2022, diesmal mit diesen Themen:

* ask! Netzwerktagung: Anmeldung und Programm-Update
* VHS-Kurs "Ambulante Krisenbegleitung" in Weil am Rhein

### ask! Netzwerktagung: Anmeldung und Programm-Update

Für die ask! Netzwerktagung "Krisenbegleitung und Offener Dialog" am Sa, 25. Juni ist nun die verbindliche Anmeldung auf der Website freigeschaltet:

[https://ask-freiburg.net/tagung/](https://ask-freiburg.net/tagung/ "https\://ask-freiburg.net/tagung/")

Außerdem wurde das Programm noch um die Vorstellung des Moderator:innen-Pools von ask! und um einen Interaktiver Austausch im Plenum mit Elementen aus dem Offenen Dialog erweitert.

### VHS-Kurs "Ambulante Krisenbegleitung" in Weil am Rhein

Im Weil am Rhein ist vergangenen Freitag erfolgreich der VHS-Kurs "Ambulante Krisenbegleitung" mit Michael Goetz-Kluth gestartet. Der Kurs wurde organisiert vom Initiativkreis\
für Ausserstationäre Krisenbgeleitung im Kreis Lörrach und zählt derzeit 11 Teilnehemende. Bis zum 2. Modul am 8.4. ist noch die Anmeldung über die VHS-Weil-Website möglich, um noch mit einzusteigen:

[https://www.vhs-weil-am-rhein.de/programm/gesellschaft.html?action%5B19%5D=course&courseId=556-C-221-10601&rowIndex=0](https://www.vhs-weil-am-rhein.de/programm/gesellschaft.html?action%5B19%5D=course&courseId=556-C-221-10601&rowIndex=0 "https\://www.vhs-weil-am-rhein.de/programm/gesellschaft.html?action%5B19%5D=course&courseId=556-C-221-10601&rowIndex=0")

Der Kurs richtet sich an Angehörige und alle Interessierten, die Betroffene in seelischen Krisen zu Hause begleiten wollen. Er bietet eine grundlegende Heranführung und Schulung zu den Themen und zur hilfreichen Umsetzung einer Krisen-Begleitung, die die Bedürfnisse und Möglichkeiten der Betroffenen und ihres persönlichen Umfelds im Fokus hat.

### Termine

* Mo, 28.03. 18:30: Trialog in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.
* 5./6. Mai: Hometreatment-Tagung in Bremen
* Sa, 7. Mai: Tag der Inklusion auf der Landesgartenschau in Neuenburg
* Sa, 25. Juni: ask! Netzwerktagung "Krisenbegleitung und Offener Dialog" im Bürgerhaus Zähringen