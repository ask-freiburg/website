---
date: 2022-05-23T23:14:28.674Z
title: "2022-04: Jubiläumsfeier: 10 Jahre ask! am 25.06., REGIO in Reutlingen am
  09.07."
---
Hallo liebe ask!-Interessierte,

Die Newsletter sind gerade im Vorfeld der Tagung ausnahmsweise doch etwas (aber nicht viel) häufiger, als alle 2 Monate... wir hoffen, das geht in Ordnung und hier kommt somit schon der vierte Newsletter des Jahres 2022, diesmal mit diesen Themen:

* Jubiläumsfeier: 10 Jahre ask! Außerstationäre Krisenbegleitung
* ask! Netzwerktagung "Krisenbegleitung und Offener Dialog"
* REGIO 2022 in Reutlingen
* Trialog Freiburg

## Jubiläumsfeier: 10 Jahre ask! Außerstationäre Krisenbegleitung

Liebe Menschen, in diesem Jahr 2022 feiert unsere Initative ask! Außerstationäre Krisenbegleitung ihr 10-jähriges Jubiläum. Wir würden uns freuen, wenn Ihr alle zusammen mit uns feiert!

**Wann:** Sa, 25.06.2022 ab 20:00 Uhr (am Abend im Nachgang der Tagung)

**Wo:** Haus 037 Vauban, Veranstaltungssaal im 1. OG (Alfred-Döblin-Platz 1, 79100 Freiburg)

Für tanzbare Live-Musik von 'smilin music' und für Finger-Food ist gesorgt.
Der Eintritt ist frei, wir freuen uns über Spenden.

Wichtig, bitte unbedingt beachten:

* Es gibt keine Parkplätze vor Ort. Einzige Parkmöglichkeit für Autos ist das kostenpflichtige Parkhaus!
* Wegen Holzparkett kein Zutritt mit Pfennigabsätzen (High Heels)!

## ask! Netzwerktagung "Krisenbegleitung und Offener Dialog"

Noch 32 Tage bis zur ersten ask! Netzwerktagung "Krisenbegleitung und Offener Dialog" mit Ralf Bohnert, Volkmar Aderhold und Initiativen für Angebote des Offenen Dialogs. Die Anmeldung für die Tagung am Sa, 25. Juni läuft und es gibt noch freie Plätze:

https://ask-freiburg.net/tagung/

## REGIO 2022 in Reutlingen

Unter dem Motto

> Mittendrin – doch außen vor (Was heißt hier eigentlich Inklusion?)

findet am Sa, 9. Juli die REGIO 2022 in Reutlingen statt.

Anmeldung und weitere Informationen gibt es unter
<https://www.gemeindepsychiatrie-bw.de/2022/02/regio-2022/>

## Trialog Freiburg

Trialog ist keine therapeutische Methode, sondern eine neue Form der Beteiligungskultur. Im Trialog gehen Psychiatrieerfahrene, Angehörige und in der Psychiatrie Tätige als Experten in eigener Sache aufeinander zu, um voneinander zu lernen.

Der Freiburger Trialog trifft sich in der Regel jeden letzten Montag im Monat um 18.30 Uhr in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.

Nächster Termin: Mo, 30. Mai um 18:30 Uhr

Weitere Infos: <https://www.fhgev.de/gruppen/trialog-in-freiburg.html>

## Termine

* Mo, 30.05. 18:30: Trialog in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.
* Sa, 25. Juni: ask! Netzwerktagung "Krisenbegleitung und Offener Dialog" im Bürgerhaus Zähringen, 9:00 bis 17:30 Uhr
* Sa, 25. Juni: Jubiläumsfeier: 10 Jahre ask! Außerstationäre Krisenbegleitung im Haus 037 im Vauban, ab 20:00 Uhr
* Sa, 9. Juli: REGIO 2022 in Reutlingen. Motto: Mittendrin – doch außen vor (Was heißt hier eigentlich Inklusion?)