---
date: 2022-10-31T09:22:14.880Z
title: "2022-07: ask! im GPV für einen Freiburger Krisendienst, ask! Treffen für Neue"
---
Hallo liebe ask!-Interessierte,

In diesem Newsletter wollen wir über die bisherigen Bemühungen von ask! in der freiburger Lokalpolitik berichten. Wir kämpfen schon lange für einen Krisendienst im Raum Freiburg und auch wenn wir noch nicht am Ziel sind, war und ist so einiges in Bewegung, was wir gerne mit euch teilen möchten.

Hier kommt also der siebte Newsletter des Jahres 2022, diesmal mit diesen Themen:

* ask! im GPV für einen Freiburger Krisendienst: aktueller Stand
* Geschichte eines Kooperationsversuches
* ask! Treffen für Neue am Mi, 09. November um 19:00 Uhr
* Termine


ask! im GPV für einen Freiburger Krisendienst: aktueller Stand
--------------------------------------------------------------

Eines der Ziele von ask! ist es, einen Krisendienst im Raum Freiburg verfügbar zu machen (siehe <https://www.ask-freiburg.net/ziele/> ). Deshalb engagieren wir uns schon seit 2015 im Gemeindepsychiatrischen Verbund (GPV) Freiburg, um die Idee eines ambulanten Krisendienstes in der Freiburger Lokalpolitik Wirklichkeit werden zu lassen.

Schon vor zwei Jahren wurde aus einem von ask! initierten Arbeitskreis des GPV heraus zusammen mit den Psychiatrie-Koordinator:innen der Stadt Freiburg und des Landkreises

* ein Projektplan für den Krisendienst erstellt
* ein Finanzantrag bei Aktion Mensch vorbereitet
* ein Kooperationsvertrag zwischen ask!, Start e.V. und Caritas erstellt, damit diese drei Akteure in Kooperation den vorbereiteten Finanzantrag bei Aktion Mensch stellen können

Leider ist der Kooperationsvertrag bis heute nicht unterschrieben und der Finanzantrag noch nicht gestellt -- die Zusammenarbeit mit der Caritas erweist sich als sehr träge und holprig.

Das ist schade, zumal die Psychiatriekoordinator:innen und praktisch alle Akteure im GPV zuletzt sehr angetan von unserem Krisendienst-Konzept waren. Gerade bleibt uns nur zu hoffen, dass die Caritas wirklich in unserem Sinne aktiv wird und wir gemeinsam an einem Strang ziehen, um das ganze doch noch zu schaffen.

Inzwischen hat sich jedoch auch die politische und damit verbundene finanzielle Situation geändert: auf Landesebene sind 6 Mio. Euro für die Einrichtung telefonischer Krisendienste in ganz Baden Württemberg in Aussicht gestellt. Einerseits ist dies erfreulich, weil sich endlich eine Finanzierung aus öffentlicher Hand für ambulante Krisenversorgung abzeichnet, andererseits führt dies von Landesseite zu rein telefonischen "Krisendiensten", was weder dem Konzept von ask! noch dem vom GPV Freiburg und Landkreis entspricht. Für Freiburg ist ein aufsuchender und begleitender Krisendienst geplant.

Daraus ergibt sich die Aufgabe für die Planungsgruppe des Krisendienstes, ein Finanzierungsmodell zu erstellen, in dem die Finanzierung durch Landesmittel (ergänzt durch kommunale Mittel) mit einer Finanzierung für die aufsuchende Arbeit plus Anlaufstelle (z.B. durch Aktion Mensch) kombiniert werden kann.


Geschichte eines Kooperationsversuches
--------------------------------------

Ergänzend zu obigen Ausführungen hier noch ein bisschen Hintergrund-Lektüre:

Bereits 2016 wurde auf unsere Initiative hin der Arbeitskreis "Ambulante Krisenbegleitung" im Gemeindepsychiatrischen Verbund (GPV) gegründet, in dem 13 Organisationen aus dem GPV sowie die Psychiatriekoordinatorin der Stadt Freiburg, Frau Kubbutat, vertreten sind.

In diesem Arbeitskreis wurden, basierend auf dem von ask! erarbeiteten Konzept, die Bedarfe und Möglichkeiten der Realisierung eines ambulanten Krisendienstes ausgelotet. Dieser Arbeitskreis richtete wiederum eine Steuerungsgruppe zur Realisierung eines Krisendienstes ein, bei der alle GPV-Mitglieder beitreten konnten und die 2020 ihre Arbeit aufnahm. Sie besteht aus

* 2 Vertreter:innen von ask!
* den beiden Psychiatriekoordinator:innen Freiburg (Frau Kubbutat) und Landkreis (Herr Keim)
* einer:r Vertreter:in der Caritas
* eine:r Verteter:in für Start e.V.

In dieser Steuerungsgruppe wurde dann bis Mitte 2020 unter anderem die im vorigen Beitrag erwähnten Punkte für einen Krisendienst erarbeitet (Erstellung Projektplan, Vorbereitung Finanzantrag und Kooperationsvertrag).

Leider ist der Kooperationsvertrag bis heute nicht unterschrieben und noch kein Finanzantrag gestellt. Zunächst schien eine Rechtsprüfung der Caritas durch Corona stark verzögert, aber nach insgesamt über 2 Jahren -- in denen sogar bereits die von der Steuerungsgruppe geplanten Fortbildungen für zukünftige Mitarbeiter:innen des Krisendienstes durchgeführt wurden -- bekamen wir vom Vorstand der Caritas die Rückmeldung, dass die bisherige Formulierung des Kooperationsvertrags aus juristischen Gründen ungeeignet sei. Dies wurde vom Amtsleiter des Amts für Soziales, sowie dem Sozialdezernenten des Landkreises bestätigt. Beide setzen sich mittlerweile für einen konstruktiven und sinnvollen Kooperationsvertrag im Sinne des ursprünglich erarbeiteten Konzepts ein.

In der Zwischenzeit hat sich jedoch, wie im vorigen Beitrag beschrieben, auch auf Landesebene etwas in der Politik getan...

Insgesamt sieht es nun also wie folgt aus:

die Steuerungsgruppe hat im ersten halben Jahr super viel auf die Beine gestellt und konkret für den Start eines Krisendienstes vorbereitet: Runde Tische veranstaltet, Fortbildungen durchgeführt, Projektplan erstellt, ...

All das ist in den folgenden zwei Jahren im Sand -- noch nicht verlaufen aber -- gedümpelt, während der Finanzantrag bei Aktion Mensch und die Kooperationsvereinbarung weiter von Seiten der Caritas auf sich warten lassen.

Inzwischen hat sich die politische Situation geändert, und die Steuerungsgruppe muß erneut viel Kreativität investieren, damit nicht nur ein reiner telefon-Vermittlungs-"Krisendienst" eingerichtet wird, sondern einer, der dem Freiburger (ask!-) Konzept gerecht wird: einen aufsuchenden und begleitenden Krisendienst!

Sowohl für die Steuerungsgruppe als natürlich auch für uns ask!-ler:innen ist das Gesamtkonzept eines Krisendienstes mit Telefon, Anlaufstelle und mobilen, aufsuchenden Teams alternativlos und wir werden weiterhin unsere Energie für dessen Realisierung einsetzen.

Es bleibt zu hoffen, dass die Caritas in unserem Sinne aktiv wird und wir gemeinsam an einem Strang ziehen.


ask! Treffen für Neue am Mi, 09. November um 19:00 Uhr
------------------------------------------------------

Das Treffen richtet sich an Menschen, die erst seit kurzem oder noch gar nicht bei ask! dabei sind und findet statt in den Räumen des Club 55, Schwarzwaldstraße 9. Super, um mehr über ask! zu erfahren, Fragen beantwortet zu bekommen und rauszufinden, wie man sich bei ask! einbringen kann.

Interessierst du dich dafür Menschen in Krisensituationen zu unterstützen? Hast du selbst Erfahrungen (im Umgang) mit seelischen Krisen? Dann komm gerne unverbindlich zu diesem offenen Treffen!

Generell ist das Treffen interessant für alle Menschen (mit und ohne Psychiatrie-Erfahrung sowie Angehörige)

* die unsere Arbeit unterstützen möchten
* die KrisenbegleiterInnen werden möchten
* die sich ausführlicher über die Arbeit von ask! informieren möchten


Termine
-------

* Mo, 31.10. 18:30: Trialog -- FÄLLT DIESEN MONAT AUS!
* Mi, 09.11. 19:00: ask! Treffen für Neue/Interessierte in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG. 