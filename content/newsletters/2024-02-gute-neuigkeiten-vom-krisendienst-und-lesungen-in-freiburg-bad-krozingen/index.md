---
date: 2024-10-08T13:35:44.004Z
title: "2024-02: Gute Neuigkeiten vom Krisendienst und Lesungen in Freiburg +
  Bad Krozingen"
---
Hallo liebe ask!-Interessierte,

Hier kommt der zweite Newsletter des Jahres 2024. Diesmal mit folgenden Themen:

* Gute Neuigkeiten vom Krisendienst Regio
* Lesungen von Lea De Gregorio: "Unter Verrückten sagt man Du" in Freiburg und Bad Krozingen
* Fortbildung Offener Dialog 2025 nochmal in Freiburg
* Radiosendung ‘VielFalter’
* Termine

### Gute Neuigkeiten vom Krisendienst Regio

Seit dem letzten Wochenende wissen wir endlich, daß der Antrag zur Anschubfinanzierung des Krisendienstes bei Aktion Mensch angenommen wurde, juchhu!

Wir sind also einen deutlichen Schritt weitergekommen und können ab sofort mit den Vorbereitungen starten.

Praktisch fangen wir trotzdem in mancherlei Hinsicht fast wieder bei Null an. Denn wir können kaum davon ausgehen, dass die Interessierten, die wir schon vor der Coronapandemie und auch währenddessen fortgebildet hatten, nach so langer Zeit alle noch parat stehen.

In Absprache mit den Psychiatriekoordinatoren von Stadt und Landkreis werden wir daher vorausssichtlich nochmals Kurzfortbildungen anbieten in den nächsten Monaten. Auch die bereits Fortgebildeten werden demnächst angeschrieben. Denn nun gilt es in absehbarer Zeit möglichst viele Menschen zu finden, die nebenberuflich oder ehrenamtlich den Telefondienst an den Wochenenden übernehmen wollen.

Wir suchen außerdem nach einer zweiten Teilzeitstelle für die Koordinationsleitung des Krisendienstes. Bitte bei Interesse gerne an kontakt@ask-freiburg.net schreiben, wir verschicken auch gerne eine Stellenbeschreibung!

Und sonst wird es in nächster Zeit auch noch viel zu organisieren geben: es braucht Schreibtisch und PC und Telefone, eine eingängige Telefonnummer, Lohnabrechnungsstelle, Abschließen von Versicherungsverträgen u.v.m.

Aber mit all dem können wir jetzt loslegen und sobald es weitere Infos gibt -- z.B. wann es etwa die ersten Angebote des Krisendienstes geben wird und wie die genau aussehen -- informieren wir natürlich nochmal.

### Lesungen von Lea De Gregorio: "Unter Verrückten sagt man Du" in Freiburg und Bad Krozingen

im Rahmen der Woche der Seelischen Gesundheit gibt es zwei Lesungen mit Lea De Gregorio:\
\
am Sonntag 13. Oktober um 16:00 Uhr\
in der Mediathek Bad Krozingen (Bahnhofsstraße 3b, 79198 Bad Krozingen)\
\
und\
\
am Montag 14. Oktober um 19:30 Uhr\
in der Stadtbibliothek Freiburg (Münsterplatz 17, 79098 Freiburg)

Lea De Gregorio liest aus Ihrem 2024 erschienenen Buch "Unter Verrückten sagt man Du":

An einer Umbruchstelle im Leben wird Lea De Gregorio verrückt. Zu viele Gedanken drehen frei in ihrem Kopf, zu viele Fragen rasen ihr durchs Herz, der Schlaf bleibt aus. Und es folgt, was hierzulande nun mal vorgesehen ist: die Behandlung in der Psychiatrie. Doch geht der Heilung die Entmündigung voraus. Hier bestimmen, entscheiden, sprechen andere für sie.\
Muss sie sich dieser althergebrachten Ordnung tatsächlich fügen, damit alles besser wird? Oder sie erst recht in Frage stellen? Eine Suche nach grundlegenden Antworten beginnt, sie führt sie an tabuisierte Orte der Geschichte, in unsere Sprache, die Philosophie und schließlich in den Kampf. Gegen Ausgrenzung und Diskriminierung von Verrückten, einer viel zu lange übersehenen Minderheit.\
Lea De Gregorio entlarvt die tradierten Ungerechtigkeitenin unserem Denken, Fühlen, Handeln. Unter Verrückten sagt man du leistet dringend notwendige Psychiatrie- und Gesellschaftskritik. In einer Sprache, die so klar und so klug und so zärtlich ist, dass sie den Blick auf unser Zusammenleben substanziell zu verändern vermag.

### Fortbildung Offener Dialog 2025 nochmal in Freiburg

Die Systemische Fortbildung: "Offener Dialog - Sozialraumorientierte Netzwerkarbeit" kann auch 2025 nochmal in Freiburg angeboten werden! Die Weiterbildung wird ausgerichtet durch ask! e.V. Freiburg und startet im Oktober 2025. Mehr Informationen und die Möglichkeit zur Anmeldung gibt es unter\
\
<https://www.ask-freiburg.net/fortbildung-offener-dialog/>

### Radiosendung ‘VielFalter’

Es gibt eine neue Ausgabe der Radiosendung ‘VielFalter’ auf Radio Dreyeckland:\
In der ersten Hälfte der Sendung gibt es einen Beitrag von Radio Loca (Querfunk Karlsruhe) zum Drogenkonsumraum in Freiburg, in der zweiten Hälfte geht es darum „Warum die Ausweitung des medizinischen Zwangs keine Option sein kann“.

Die Sendung findet  ihr unter <https://rdl.de/beitrag/vielfalter-magazin-f-r-polyphonie-und-selbstbestimmung-07102024> zum Herunterladen und Anhören.

### Termine

* Do, 10.10. bis So, 20.10.: Woche der Seelischen Gesundheit <https://www.freiburg.de/pb/530411.html>
* Sonntag 13.10. um 16:00 Uhr: Lesung "Unter Verrückten sagt man Du" von Lea De Gregorio in der Mediathek Bad Krozingen
* Montag 14.10. um 19:30 Uhr: Lesung "Unter Verrückten sagt man Du" von Lea De Gregorio in der Stadtbibliothek Freiburg
* Mo, 28.10. um 18:30 Uhr: Trialog, in den Räumen der FHG/Club 55, Schwarzwaldstraße 9 im 2. OG.
* Do, 14.11. bis Sa, 16.11.: DGSP Jahrestagung 2024 in Freiburg (siehe <https://www.dgsp-ev.de/veranstaltungen/aktuelle-tagungen/jahrestagung-2024> )
* Fr, 4. April 2025: REGIO-Tagung in Freiburg (siehe <https://www.gemeindepsychiatrie-bw.de/2024/04/save-the-date-cloned-regio-2024/> )