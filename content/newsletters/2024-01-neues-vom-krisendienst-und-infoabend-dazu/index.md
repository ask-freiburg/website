---
date: 2024-06-04T18:56:37.153Z
title: "2024-01: Neues vom Krisendienst und Infoabend dazu"
---
Hallo liebe ask!-Interessierte,

Hier ist er nun, der erste Newsletter des Jahres 2024. Sogar noch in der ersten Jahreshälfte und mit folgenden Themen:

* Aktueller Stand Krisendienst Regio
* ask! stellt sich vor: Infoabend am Mi, 12.06.2024 um 17 Uhr
* Radiosendung VielFalter vom Juni 2024
* Termine

### Aktueller Stand Krisendienst Regio

Die Krisendienst Regio gGmbH ist nun auch Mitglied im Paritätischen und somit sind inzwischen alle Voraussetzungen gegeben, einen Antrag bei Aktion Mensch für die Anschubfinanzierung des Krisendienstes zu stellen. Erfreulicherweise wurde Anfang des Jahres der Höchstbetrag für Förder-Anträge bei Aktion Mensch auf 400000 EUR erhöht. Da die einzubringenden Eigenmittel für das geförderte Projekt nach wie vor 10% sind, müssen dafür nun allerdings auch insgesamt 40000 EUR an Eigenmitteln aufgebracht werden. Diese sollen bei den Mitgliedern des GPV eingeworben werden.\
Inzwischen ist der Antrag raus (Februar 2024), kam allerdings zurück, da wir noch einiges umformulieren sollten; nun ist er zum 2. Mal im Mai 2024 losgeschickt worden. Anfang Juni starten wir mit Kennenlerngesprächen mit InteressentInnen für die Geschäftsführung/ Koordinierungsstelle. Solange wir kein verbindliches OK von Aktion Mensch haben können wir auch keine Personen verbindlich anstellen, daher sind wir noch immer nicht damit an die Öffentlichkeit getreten. Im April fand nochmals eine Fortbildung mit Ralf Bohnert statt; die Kosten hierfür wurden erstmals über die Krisendienst Regio gGmbH abgerechnet, die die bewilligten Zuschüsse von Stadt und Landkreis dafür (und hoffentlich bald für weitere anstehende Sachkosten) erstmals abgerufen hat. Wir hoffen,\
dass das Warten bald ein erfreuliches Ende haben wird...

### ask! stellt sich vor: Infoabend am Mi, 12.06.2024 um 17 Uhr

Am Mittwoch, 12.06.2024 um 17:00 Uhr findet in den Räumen der Freiburger Hilfsgemeinschaft (FHG -- Schwarzwaldstraße 9, 79117 Freiburg im 2. OG) ein Infoabend statt.

Es ist ein Treffen für alle Interessierten und richtet sich an Menschen, die erst seit kurzem oder noch gar nicht bei ask! dabei sind. Es wird um die Arbeit von ask! gehen und auch den entstehenden Krisendienst für die Region Freiburg und wie der Krisendienst mit der Arbeit von ask! zusammenhängt.

Wir geben einen Überblick über unsere Aktivitäten wie Krisenbegleitung, Krisendienst für Freiburg, Hintergrundinformationen zu unserem außerstationären Ansatz sowie Krisenverständnis und zeigen Möglichkeiten zur Mitwirkung bei ask! auf.

Generell ist das Treffen interessant für alle Menschen (mit und ohne Psychiatrie-Erfahrung sowie Angehörige)

* die unsere Arbeit unterstützen möchten
* die KrisenbegleiterInnen werden möchten
* die sich ausführlicher über die Arbeit von ask! informieren möchten
* die Interesse haben, sich beim entstehenden Krisendienst für die Region Freiburg einzubringen (und ggf. auch schon an der zugehörigen Fortbildung im April teilgenommen haben)

### Radiosendung VielFalter vom Juni 2024

In der Sendereihe "VielFalter - Magazin für Polyphonie und Selbstbestimmung" auf Radio Dreyeckland gibt es diesmal ein Gespräch mit Lea De Gregorio, der Autorin von dem Buch "Unter Verrückten sagt man Du". Die Sendung kann hier angehört werden:

<https://rdl.de/beitrag/vielfalter-magazin-f-r-polyphonie-und-selbstbestimmung-03062024>

### Termine

* Mi, 12.06. um 17 Uhr: Infoabend ask! und Krisendienst (siehe oben)
* Mo, 24.06. um 18:30 Uhr: Trialog, in den Räumen der FHG/Club 55, Schwarzwaldstraße 9 im 2. OG.
* Do, 10.10. bis So, 20.10.: Woche der Seelischen Gesundheit
* Do, 14.11. bis Sa, 16.11.: DGSP Jahrestagung 2024 in Freiburg (siehe <https://www.dgsp-ev.de/veranstaltungen/aktuelle-tagungen/jahrestagung-2024> )
* Fr, 4. April 2025: REGIO-Tagung in Freiburg (siehe <https://www.gemeindepsychiatrie-bw.de/2024/04/save-the-date-cloned-regio-2024/> )