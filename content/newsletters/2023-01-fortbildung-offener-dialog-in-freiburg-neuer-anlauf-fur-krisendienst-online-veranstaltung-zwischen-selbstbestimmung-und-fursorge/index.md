---
date: 2023-01-24T19:10:51.188Z
title: '2023-01: Fortbildung Offener Dialog in Freiburg, Neuer Anlauf für
  Krisendienst, Online-Veranstaltung "Zwischen Selbstbestimmung und Fürsorge"'
---
Hallo liebe ask!-Interessierte,

Ein gutes Neues Jahr euch allen! Das Jahr ist noch jung, aber es tut sich schon so einiges beim Offenen Dialog und nun auch wieder beim Krisendienst in Freiburg...

Hier kommt der erste Newsletter des Jahres 2023, diesmal mit diesen Themen:

* Fortbildung Offener Dialog mit Volkmar Aderhold ab Mai 2023 in Freiburg
* ask! im GPV für einen Freiburger Krisendienst: was es Neues gibt
* Online-Veranstaltung am 05. April 2023: Zwischen Selbstbestimmung und Fürsorge: Wie gelingt „freies Handeln” im Kontext psychiatrischer Behandlung
* Online-Veranstaltung am 08. Februar: Was hält mich aus, wenn ich mein Leben nicht mehr aushalte?
* Termine

### Fortbildung Offener Dialog mit Volkmar Aderhold ab Mai 2023 in Freiburg

Beginnend im Mai 2023 wird ask! e.V in Freiburg zum zweiten Mal die Fortbildung Offener Dialog ausrichten. Kursleiter ist Dr. Volkmar Aderhold gemeinsam mit weiteren Trainer:innen.

Kursdaten und Anmeldemöglichkeit finden sich auf unserer Website:

<https://ask-freiburg.net/fortbildung-offener-dialog/>

Die Fortbildung besteht aus **9 Workshops mit je 2 Tagen**. Die Stundenzahl beträgt 16 h pro Einheit = 144 h insgesamt. Die Teilnehmerzahl ist auf maximal 30 beschränkt.

**Systemische Fortbildung in Familien- und Netzwerktherapie**

Können Sie sich ein psychiatrisches Versorgungssystem vorstellen, in der das erste Treffen bei einer Psychose oder anderen schweren Krise innerhalb eines Tages organisiert wird? In der sowohl der Patient als auch die Familie am ersten Treffen und am Behandlungsverfahren für so lange wie nötig teilnehmen dürfen? In der - von der medizinischen Grundversorgung über die Psychiatrie bis hin zum Sozialamt – alle Mitarbeiter, die mit der Familie zu tun haben, zu denselben Treffen eingeladen werden und offen ihre Meinungen darüber austauschen, wie die Krise entstanden ist und was getan werden sollte? Dass die Mitarbeiter dieselben bleiben, solange Hilfe gebraucht wird? Und dass alle Diskussionen und Entscheidungen über die Behandlung im Beisein des Patienten und der Familie geschehen?

In der **bedürfnisangepassten Behandlung** (Yrjö Alanen und Team) bilden Netzwerkgespräche - von Anfang an und möglichst kontinuierlich - die zentrale Achse der Behandlung. Weitere therapeutische Verfahren kommen je nach den individuellen Bedürfnissen der Patienten hinzu.

Der **Offene Dialog** (Jaakko Seikkula und Team) hat für die Praxis dieser Netzwerkgespräche eine spezifische Methodik entwickelt, für die einzelne sog. Schlüsselelemente formuliert wurden.

Die Fortbildung möchte vor allem diese systemisch dialogische Gesprächskompetenz vermitteln. Dafür werden die einzelnen Elemente dieser Gesprächsführung eingeübt, die auch in anderen Zusammenhängen, wie z.B. Einzelgesprächen wirksam eingesetzt werden können. Ein zentrales Element ist dabei das Reflektieren.

Den Hintergrund dieser dialogischen Praxis bildet eine **therapeutische Grundhaltung**, die von der Annahme einer fortlaufenden dialogischen Konstruktion von Wirklichkeit (sozialer Konstruktionismus), unaufhebbarer Vielstimmigkeit  (Polyphonie), notwendiger Toleranz von Unsicherheit sowie Prozess- und Ressourcenorientierung gekennzeichnet ist. Die Entwicklung dieser Haltung wird von vielen Teilnehmern ebenfalls als wichtiger Teil der Fortbildung erachtet.

Bereits einzelne **Netzwerkgespräche** besitzen oft eine verstärkende Wirkung für andere Therapien. Die konsequente Anwendung des Modells kann die fortschreitende ambulante Ausrichtung der psychiatrischen Arbeit einer Region erleichtern. Im Rahmen der „Integrierten Versorgung“ bewirkt dieses Vorgehen eine deutliche Verringerung der Hospitalisierung, eine verbesserte Krisenbewältigung im Lebensfeld und längerfristige Krisenprävention, sowie eine kooperative Vernetzung mit anderen therapeutischen Angeboten und sozialraumorientiertes Arbeiten.

Ebenso bei Klient*innen, die im Rahmen des **Betreuten Wohnens, in Tagesstätten oder Wohnheimen** betreut werden, können oftmals unerwartete positive Entwicklungen entstehen. Die dialogische Praxis fördert darüber hinaus die Teamentwicklung und kooperative Kompetenz der Mitarbeiter.

Weitere Infos unter <https://ask-freiburg.net/fortbildung-offener-dialog/>

### ask! im GPV für einen Freiburger Krisendienst: was es Neues gibt

Die im letzten Newsletter beschriebenen Entwicklungen zum Freiburger Krisendienst können inzwischen um ein weiteres kleines Kapitel ergänzt werden:

Die recht überraschend in Aussicht gestellten 6 Mio Euro landesweit für (Telefon-)Krisendienste gibt es nun leider doch nicht. Aufgrund dieser weggefallenen Landeszuschüsse steht die Caritas inzwischen nicht mehr als Kooperationspartnerin für einen Freiburger Krisendienst zur Verfügung. In der AG Krisendienst des GPV wurde nun erneut nach Kooperationspartnern gesucht, und es soll nun eine neue Kooperation aus

* ask! e.V.
* Freiburger Hilfsgemeinschaft e.V. (FHG)
* Start e.V.

für einen Freiburger Krisendienst gegründet werden. Menschen aus allen drei Vereinen werden sich noch im Januar treffen, um weitere Einzelheiten zu klären. Ein bereits gestellter Antrag für Mittel zum Krisendienst im kommenden Haushalt der Stadt Freiburg bleibt bestehen. Ebenso hoffen wir, dass die Arbeit der vergangenen Jahre zumindest als Vorarbeit für diesen neuen Anlauf dann noch ihre Früchte trägt und Ende 2023 der Aufbau des Krisendienstes für Freiburg beginnen kann.

### Online-Veranstaltung am 05. April 2023: Zwischen Selbstbestimmung und Fürsorge: Wie gelingt „freies Handeln” im Kontext psychiatrischer Behandlung

Gerne geben wir folgende Einladung des Liga-BW im Rahmen unseres Newsletters weiter:

Wir freuen uns Ihnen mitzuteilen, dass nach der krankheitsbedingten Absage unsere Online-Veranstaltung **Zwischen Selbstbestimmung und Fürsorge: Wie gelingt „freies Handeln” im Kontext psychiatrischer Behandlung** im Rahmen der Veranstaltungsreihe des Liga-UA ‚Psychiatrie ‚WISSEN FÜR DIE PRAXIS - Digitale Fachimpulse Sozialpsychiatrie‘ ein neuer Termin gefunden wurde.

Mittwoch, 05. April 2023\
15:00 Uhr bis 16:30 Uhr

**Inhalt**

Das Spannungsfeld zwischen Selbstbestimmung und Fürsorge prägt das Erleben und den Umgang psychisch erkrankter Menschen mit ihrer Erkrankung.

Catharina Flader und Dr. Gustav Wirtz erarbeiten im Dialog unterschiedliche Perspektiven auf das Thema und greifen widersprüchliche Haltungen in der Öffentlichkeit bis hin zu Meinungen im persönlichen, beruflichen und unterstützenden Umfeld sowie in der öffentlichen Verwaltung und dem Gesundheitswesen auf. Ziel ist es, das Spannungsfeld von Selbstbestimmung und Fürsorge darzustellen und zu diskutieren.

weitere Infos und Einladung mit Zugangsdaten:

<https://liga-bw.de/zwischen-selbstbestimmung-und-fuersorge-wie-gelingt-freies-handeln-im-kontext-psychiatrischer-behandlung/>

### Online-Veranstaltung am 08. Februar: Was hält mich aus, wenn ich mein Leben nicht mehr aushalte?

Gerne geben wir auch diese Einladung des Liga-BW im Rahmen unseres Newsletters weiter:

im Rahmen der Veranstaltungsreihe des Liga-UA ‚Psychiatrie ‚WISSEN FÜR DIE PRAXIS - Digitale Fachimpulse Sozialpsychiatrie‘ laden wir Sie zu einer weiteren kostenlosen Online-Veranstaltung ein.\
 \
**Was hält mich aus, wenn ich mein Leben nicht mehr aushalte?**\
 \
Mittwoch, 08. Februar 2023\
15:00 bis 16:30 Uhr\
\
**Inhalt**\
Suizidalität ist immer noch ein Tabu. Die Veranstaltung soll dazu beitragen, dieses Tabu zu brechen. denn es ist wichtig diesem Thema nicht auszuweichen, sondern es anzusprechen.\
Melanie Schock wird ihr Erfahrungswissen aus der Perspektive der eigenen Betroffenheit zu Suizidalität einbringen. Carina Kebbel bringt die Perspektive als Außenstehende ein, die mit dem Thema Suizid konfrontiert wurde. Ziel des dialogischen Gespräches ist es andere Menschen zu sensibilisieren, Fehlannahmen und Ängste zu beseitigen und zu vermitteln, was von Menschen in dieser Situation hilfreich und unterstützend wahrgenommen wird. Darüber hinaus wird auch aktuelles Faktenwissen einfließen.

weitere Infos und Einladung mit Zugangsdaten:

<https://liga-bw.de/was-haelt-mich-aus-wenn-ich-mein-leben-nicht-mehr-aushalte/>

### Termine

* Mo, 30.01. 18:30: Trialog, in den Räumen der FHG/Club 55, Schwarzwaldstraße 9 im 2. OG.
* Mi, 08.02. 15:00: Online-Veranstaltung: Was hält mich aus, wenn ich mein Leben nicht mehr aushalte?
* Mi, 05.04. 15:00: Online-Veranstaltung: Zwischen Selbstbestimmung und Fürsorge: Wie gelingt „freies Handeln” im Kontext psychiatrischer Behandlung