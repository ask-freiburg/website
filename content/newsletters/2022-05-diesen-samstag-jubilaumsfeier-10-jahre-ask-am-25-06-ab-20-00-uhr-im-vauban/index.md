---
date: 2022-06-22T16:00:43.157Z
title: "2022-05: Diesen Samstag: Jubiläumsfeier 10 Jahre ask! am 25.06. ab 20:00
  Uhr im Vauban"
---
Hallo liebe ask!-Interessierte,

Diese Woche am Samstag ist es soweit: die Jubiläumsfeier von ask! findet statt und am selben Tag auch die erste ask!-Netzwerktagung, auf die wir uns schon so lange vorbereiten!
Wer schon länger auf dem Newsletter ist, weiß natürlich schon Bescheid, aber für alle neu hinzugekommenen -- und auch einfach weil es so eine große Sache für uns ist -- hier nochmal ein (letzter) Newsletter zu dem Thema, bevor sich die Newsletter-Häufigkeit dann wieder (auf die gewohnten 6-8mal pro Jahr) normalisieren wird

* Jubiläumsfeier: 10 Jahre ask! Außerstationäre Krisenbegleitung
* ask! Netzwerktagung "Krisenbegleitung und Offener Dialog"
* REGIO 2022 in Reutlingen
* Trialog Freiburg

## Jubiläumsfeier: 10 Jahre ask! Außerstationäre Krisenbegleitung

Im Jahr 2022 feiert unsere Initative "ask! Außerstationäre Krisenbegleitung" ihr 10-jähriges Jubiläum! Wir möchten euch herzlich einladen mitzufeiern:

Am Samstag, 25.06.2022 ab 20:00 Uhr
im Haus 037 Vauban, Veranstaltungssaal im 1. OG (Alfred-Döblin-Platz 1, 79100 Freiburg)

Auch wenn an diesem Samstag schon einiges an Programm in Freiburg geboten ist (nicht nur mit unserer ask! Netzwerktagung, sondern auch CSD, Freiburg stimmt ein, Habs-Fest-Flohmarkt usw...), behaltet euch noch ein bisschen Energie-Reserven für den Abend auf, um gemeinsam auf die vergangenen 10 und die kommenden X Jahre anzustoßen, ins Gespräch zu kommen, und der (durchaus tanzbaren) Live-Musik von smilin music zu lauschen. Für Finger-Food ist gesorgt, der Eintritt ist frei (wir freuen uns natürlich über Spenden).

Bitte beachten:

* Es gibt keine Parkplätze vor Ort. Einzige Parkmöglichkeit für Autos ist das kostenpflichtige Parkhaus!
* Wegen Holzparkett kein Zutritt mit Pfennigabsätzen (High Heels)!

Wer zum Abendessen grossen Hunger mitbringt, kann z.B. ‘im Süden" essen, unsere Feier findet im gleichen Gebäude statt.

## ask! Netzwerktagung "Krisenbegleitung und Offener Dialog"

Noch 3 Tage bis zur ersten ask! Netzwerktagung "Krisenbegleitung und Offener Dialog" mit Ralf Bohnert, Volkmar Aderhold und Initiativen für Angebote des Offenen Dialogs. Wir freuen uns, alle, die sich angemeldet haben, am kommenden Samstag im Bürgerhaus Zähringen in Freiburg begrüßen zu dürfen.

## REGIO 2022 in Reutlingen

Unter dem Motto

> Mittendrin – doch außen vor (Was heißt hier eigentlich Inklusion?)

findet am Sa, 9. Juli die REGIO 2022 in Reutlingen statt.

Anmeldung und weitere Informationen gibt es unter
<https://www.gemeindepsychiatrie-bw.de/2022/02/regio-2022/>

## Trialog Freiburg

Trialog ist keine therapeutische Methode, sondern eine neue Form der Beteiligungskultur. Im Trialog gehen Psychiatrieerfahrene, Angehörige und in der Psychiatrie Tätige als Experten in eigener Sache aufeinander zu, um voneinander zu lernen.

Der Freiburger Trialog trifft sich in der Regel jeden letzten Montag im Monat um 18.30 Uhr in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.

Nächster Termin: Mo, 27. Juni um 18:30 Uhr

Weitere Infos: https://www.fhgev.de/gruppen/trialog-in-freiburg.html

## Termine

* Sa, 25. Juni: ask! Netzwerktagung "Krisenbegleitung und Offener Dialog" im Bürgerhaus Zähringen, 9:00 bis 17:30 Uhr
* Sa, 25. Juni: Jubiläumsfeier: 10 Jahre ask! Außerstationäre Krisenbegleitung im Haus 037 im Vauban, ab 20:00 Uhr
* Mo, 27. Juni um 18:30: Trialog in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.
* Sa, 9. Juli: REGIO 2022 in Reutlingen. Motto: Mittendrin – doch außen vor (Was heißt hier eigentlich Inklusion?)