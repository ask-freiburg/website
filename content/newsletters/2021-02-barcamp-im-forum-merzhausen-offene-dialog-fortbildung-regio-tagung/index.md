---
date: 2021-06-14T22:50:33.792Z
title: "2021-02: BarCamp im Forum Merzhausen, Offene Dialog Fortbildung, REGIO-Tagung"
---
Hallo liebe ask!-Interessierte,

hier kommt der zweite ask!-Newsletter 2021, diesmal mit einer kleinen Sammlung von regionalen und überregionalen Veranstaltungen, auf die wir gerne aufmerksam machen wollen:

* BarCamp im Forum Merzhausen (03. Juli)
* Offener Dialog Fortbildung Koblenz 2022/2023
* REGIO-Tagung 2021 in Stuttgart (02. Juli)
* Radiosendung ‘VielFalter’

## BarCamp im Forum Merzhausen

Am 03. Juli findet ein BarCamp im Forum Merzausen statt (mehr dazu in der Einladung unten). Ab sofort ist die Anmeldung dazu möglich unter

<https://www.freiburg-macht-zukunft.de/anmelden.html>

Termin: Samstag, 3. Juli 2021\
von 9:30 bis 16:00 Uhr\
Ort: Forum Merzhausen

Aus der Einladung:

Eine Vielzahl ökologisch, sozial und gesellschaftlich progressiver Initiativen engagieren sich für die Bearbeitung der drängenden Krisen:\
Klimakrise, Artensterben, Vertrauenskrise der Demokratie, zunehmende Armut und Verdrängung benachteiligter Menschen, Diskriminierung u. A. sind Phänomene einer multiplen Krise. Trotz zahlloser Bekenntnisse der Politik zu mehr Klimaschutz und sozialer Gerechtigkeit: die Krisen verschärfen sich weiter. Zahlreiche Freiburger*innen setzen sich mit großem Einsatz für eine bessere, zukunftsfähige Gesellschaft ein. Doch ist es bislang keinem der Akteure gelungen die notwendigen strukturellen Veränderungen zu erreichen. Aus diesem Befund folgern wir, dass wir Kräfte bündeln, Macht und Ressourcen generieren und strategisch einsetzen müssen, um gemeinsam den Wandel einzuleiten.
Die Herausforderung könnte kaum größer sein: Freiburg benötigt einen umfassenden sozial-ökologischen Umbau. Hierzu gehört die schnelle Klimaneutralität, Klimawandelanpassungsmaßnahmen, Mietengerechtigkeit sowie eine inklusive und diskriminierungsfreie Sozialpolitik.

Um die Kooperation zwischen der Vielzahl der bestehenden Initiativen zu intensivieren, findet am 3. Juli 2021 wir eine [Barcamp- bzw. Open Space Veranstaltung](https://de.wikipedia.org/wiki/Barcamp) statt, mit dem Ziel, viele Menschen aus möglichst vielen Gruppen zu versammeln, um gemeinsam Transformationsziele und -projekte auszuarbeiten und deren Umsetzung vorzubereiten. Das Ziel: ein tiefgreifender Wandel Freiburgs auf dem Weg zur sozial- und klimagerechten Stadt.

Wir freuen uns auf den Austausch, die Vernetzung und viele spannende Projekte.

Zoe, Niklas, Stefan, Tobias und Christian
für das Bar-Camp-Orga-Team

## Offener Dialog Fortbildung Koblenz 2022/2023

In Koblenz wird es 2022/2023 eine Fortbildung zum Offenen Dialog geben.

Die Fortbildung besteht aus 9 Workshops mit je 2 Tagen. Die Stundenzahl beträgt 16 h pro Einheit = 144 h insgesamt.

Frühbucherrabatt bis 31.12.2021

Aus der Ankündigung:

In der bedürfnisangepassten Behandlung (Yrjö Alanen und Team) bilden Netzwerkgespräche - von Anfang an und möglichst kontinuierlich - die zentrale Achse der Behandlung einer Psychose oder anderen schweren Krise. Weitere therapeutische Verfahren kommen je nach den individuellen Bedürfnissen der Patienten hinzu.

Der Offene Dialog (Jaakko Seikkula und Team) hat für die Praxis dieser Netzwerkgespräche eine spezifische Methodik entwickelt, für die einzelne sog. Schlüsselelemente formuliert wurden.

Die Fortbildung möchte vor allem diese systemisch dialogische Gesprächskompetenz vermitteln. Dafür werden die einzelnen Elemente dieser Gesprächsführung eingeübt, die auch in anderen Zusammenhängen, wie z.B. Einzelgesprächen wirksam eingesetzt werden können. Ein zentrales Element ist dabei das Reflektieren.

Den Hintergrund dieser dialogischen Praxis bildet eine therapeutische Grundhaltung, die von der Annahme einer fortlaufenden dialogischen Konstruktion von Wirklichkeit (sozialer Konstruktionismus), unaufhebbarer Vielstimmigkeit  (Polyphonie), notwendiger Toleranz von Unsicherheit sowie Prozess- und Ressourcenorientierung gekennzeichnet ist. Die Entwicklung dieser Haltung wird von vielen Teilnehmern ebenfalls als wichtiger Teil der Fortbildung erachtet.

Kontakt: Julia Bröhling-Kusterer (Ivita Rheinland-Pfalz und Saarland gGmbH)

## REGIO-Tagung 2021 in Stuttgart

Am Freitag, 2. Juli 2021 findet die REGIO 2021 in Stuttgart statt.
Dieses Jahr ist das Motto

> Krisendienste – notwendiger denn je!\
> Moderne Konzepte der Krisenversorgung

Weitere Informationen, Flyer und Anmeldung sind zu finden unter

<https://www.gemeindepsychiatrie-bw.de/2021/02/regio-2021-in-stuttgart/>

## Radiosendung ‘VielFalter’:

* Die Radiosendung ‘VielFalter’ von Mirko auf Radio Dreyeckland ist inzwischen jeweils am ersten Montag des Monats von 20 bis 21 Uhr on  Air (Wiederholung tags drauf um 13 Uhr). Die Juni-Sendung 2021 findet  Ihr unter\
  <https://vielfalter.podspot.de/post/sendung-vom-7-juni-2021-mit-flora-florenz/>\
  zum Herunterladen und Anhören.

*Hinweis: Kontakt-Informationen wurden aus der Website-Version des Newsletters entfernt.*