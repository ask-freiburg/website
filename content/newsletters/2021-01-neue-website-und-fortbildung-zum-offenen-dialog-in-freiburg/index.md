---
date: 2021-01-24T10:39:48.001Z
title: "2021-01: Neue Website und Fortbildung zum Offenen Dialog in Freiburg"
---
Hallo liebe ask!-Interessierte,

ab diesem Jahr soll es wieder regelmäßiger Informationen über ask! per E-Mail geben, etwa 3-4 Newsletter pro Jahr... und hier kommt der erste mit folgenden Themen:

* Neue Website und neues Logo
* Schulung Offener Dialog und Netzwerkgespräche in Freiburg
* Weitere Themen und Termine

### Neue Website und neues Logo

ask! hat ein neues -- professionell gestaltetes -- Logo bekommen und die ask!-Website wurde passend dazu überarbeitet: ihr findet unsere neue Website unter

<https://www.ask-freiburg.net>

### Schulung Offener Dialog und Netzwerkgespräche in Freiburg

Wir freuen uns sehr, dass wir es geschafft haben, 2019 in Freiburg eine Fortbildung zum Offenen Dialog mit Volkmar Aderhold zu starten. Leider war sie von Anfang an durch organisatorisch notwendig gewordene Terminverschiebungen geprägt, vor allem durch die aktuellen Auswirkungen der Pandemie, sodass sie immer noch nicht abgeschlossen werden konnte. Da der Hauptanteil der Teilnehmenden aus unserer Regio kommt hoffen wir, dass sich dadurch die Haltung und Methodik auch bei uns rascher als bisher verbreitet und wir hoffen, dass wir einen ModeratorInnen-Pool zusammenstellen können. Dadurch wäre es möglich nicht nur institutionsintern sondern auch hier vernetzt arbeiten zu können, uns untereinander weiter unterstützen und bereichern zu können und die ask! Haltung weiter zu verbreiten. Damit dies nicht nur ehrenamtlich passiert haben wir einen Stiftungsantrag zur teilweisen Finanzierung der dafür notwendigen Organisation und der ModeratorInnen selbst gestellt, der aber noch nicht abschliessend beschieden ist.

Wir hoffen ebenfalls evtl. in 2022/23 nochmal eine weitere Fortbildung hier starten zu können.

Wer sich weitergehend für die inhaltliche Thematik interessiert kann sich vielfältig informieren. Hier als Einstieg ein Link zu einem Überblick und Aufsatz von Volkmar Aderhold [OFFENER DIALOG SUSE 2017.ppt (uni-greifswald.de)](http://www2.medizin.uni-greifswald.de/psych/fileadmin/user_upload/veranstaltungen/2017/15.-17.02.2017__Die_Subjektive_Seite_der_Schizophrenie_/Vortraege/Vortrage_17.02.2017/Aderhold_Hometreatment_und_Offener_Dialog.pdf "http\://www2.medizin.uni-greifswald.de/psych/fileadmin/user_upload/veranstaltungen/2017/15.-17.02.2017\_\_Die_Subjektive_Seite_der_Schizophrenie\_/Vortraege/Vortrage_17.02.2017/Aderhold_Hometreatment_und_Offener_Dialog.pdf") oder [APK_Tagungsband\_41\_-](https://www.apk-ev.de/fileadmin/downloads/APK_Tagungsband_41_-_Volkmar_Aderhold_-_Netzwerkgespraeche_als_Offener_Dialog.pdf "https\://www.apk-ev.de/fileadmin/downloads/APK_Tagungsband_41\_-\_Volkmar_Aderhold\_-\_Netzwerkgespraeche_als_Offener_Dialog.pdf")*[Volkmar_Aderhold](https://www.apk-ev.de/fileadmin/downloads/APK_Tagungsband_41_-_Volkmar_Aderhold_-_Netzwerkgespraeche_als_Offener_Dialog.pdf "https\://www.apk-ev.de/fileadmin/downloads/APK_Tagungsband_41\_-\_Volkmar_Aderhold\_-\_Netzwerkgespraeche_als_Offener_Dialog.pdf")*[\-_Netzwerkgespraeche_als_Offener_Dialog.pdf (apk-ev.de)](https://www.apk-ev.de/fileadmin/downloads/APK_Tagungsband_41_-_Volkmar_Aderhold_-_Netzwerkgespraeche_als_Offener_Dialog.pdf "https\://www.apk-ev.de/fileadmin/downloads/APK_Tagungsband_41\_-\_Volkmar_Aderhold\_-\_Netzwerkgespraeche_als_Offener_Dialog.pdf") und zu einem Film [https://www.youtube.com/watch?v=IsnzUxE7emI](https://www.youtube.com/watch?v=IsnzUxE7emI "https\://www.youtube.com/watch?v=IsnzUxE7emI") über die Methodik aus einem Zentrum in Skandinavien, wo diese systemische Gesprächstechnik im Kontext von psychiatrischen Zentren entwickelt und auch immer wieder erweitert wurde.

### Weitere Themen und Termine:

* Die Radiosendung 'VielFalter' von Mirko auf Radio Dreyeckland ist inzwischen jeweils am ersten Montag des Monats von 20 bis 21 Uhr on Air (Wiederholung tags drauf um 13 Uhr).\
  Die Januarsendung 2021 findet Ihr unter [https://c.web.de/@319462275247248614/unyzMo3GRsinjY4eSrEUqA](https://c.web.de/@319462275247248614/unyzMo3GRsinjY4eSrEUqA "https\://c.web.de/@319462275247248614/unyzMo3GRsinjY4eSrEUqA") zum Herunterladen und Anhören.
* Der Neuroleptika Trialog in Bern wurde auf eine Online Veranstaltungsserie umgestellt mit Vorträgen, Lesungen, Gesprächen etc. [https://www.trialogbern.ch/?p=180](https://www.trialogbern.ch/?p=180 "https\://www.trialogbern.ch/?p=180")

  Die Events werden auch aufgezeichnet und können dann im Nachhinein bei dort der Website geschaut werden.
