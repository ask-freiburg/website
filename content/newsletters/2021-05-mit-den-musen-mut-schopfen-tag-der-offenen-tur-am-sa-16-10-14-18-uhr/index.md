---
date: 2021-10-12T21:32:49.474Z
title: "2021-05: Mit den Musen Mut schöpfen: Tag der offenen Tür am Sa, 16.10.,
  14-18 Uhr"
---
Hallo liebe ask!-Interessierte,\
\
hier kommt noch eine Ergänzung zum vergangenen Newsletter, diesmal mit nur einem Thema, nämlich dieser Einladung von Schwere(s)los zum Tag der offenen Tür, bei dem sich auch ask! vorstellen wird:\
\
\
Liebe Freundinnen und Freunde von Schwere(s)Los!\
\
Wir hatten am vergangenen Sonntag eine wunderschöne Auftaktveranstaltung zur Weltwoche der Seelischen Gesundheit, bei der wir Texte von Menschen mit Krisenerfahrung und deren Angehörigen mit Musik und Tanz inszeniert haben. Bei aller Schwere des Themas gab es viele sehr berührende Momente und wir sind insbesondere den Autor*innen sehr dankbar, dass sie den Mut hatten, ihre Geschichten zu erzählen und teilweise auch selbst zu lesen und zu tanzen.\
\
Die Weltwoche ist jedoch noch nicht vorbei! Wir haben das Vergnügen, diesen Samstag, 16.10., zwischen 14 und 18 Uhr in unseren Räumen im Kleineschholzweg 5 unser therapeutisch angelegtes Projekt MuT vorzustellen. MuT steht für "Muse und Therapie" und bietet kunst-, musik-, theater- und tanz-therapeutische Gruppen- und Einzelsettings. Hervorgegangen ist MuT aus "Räume für Kunst und Therapie e.V." in der Sternwaldstraße. Das MuTige Team stellt sich erstmals beim Tag der offenen Tür öffentlich vor und bietet psychiatrieerfahrenen Menschen, ihren Angehörigen und anderen Interessierten die Möglichkeit, die Räume, die Therapeut*innen und ihre Arbeit kennen zu lernen.\
\
Parallel stellt sich bei uns die Initiative ask! vor: Der gemeinnützige Verein besteht aus Psychiatrie-Erfahrenen, Angehörigen, Professionellen und engagierten Bürger*innen. Ziel des Vereins ist, eine ambulante Alternative zur bestehenden stationären Behandlung in Kliniken aufzubauen. So soll eine bedürfnisorientierte Begleitung und Unterstützung für Menschen in seelischen Krisen realisiert werden.\
\
Es wird einige konkrete Impulse für die einzelnen künstlerischen Therapien geben:\
\
14 Uhr Beginn mit Musik und Gespräch\
             ask! stellt sich vor\
14.30 Uhr Filzen: Farbe sinnlich erleben\
15-15.45 Uhr Miniworkshop Tanz\
16 Uhr Bewegungsangebot\
17 Uhr Formen schaffen mit Ton\
17 Uhr Theater - Die Kraft der Geschichten\
\
Wer sich allgemein über das Angebot von Schwere(s)Los! informieren möchte, hat dazu auf dem Stühlinger Wochenmarkt ebenfalls am Samstag, 16.10., zwischen 10 und 13 Uhr Gelegenheit.\
\
Wir freuen uns über alle neugierigen Menschen!\
Es grüßt herzlich\
das schwerelose Team