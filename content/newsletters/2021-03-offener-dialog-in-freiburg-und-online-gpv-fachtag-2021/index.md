---
date: 2021-08-08T22:23:34.432Z
title: "2021-03: Offener Dialog in Freiburg und online, GPV-Fachtag 2021"
---
Hallo liebe ask!-Interessierte,

ganz kurz vorweg etwas in eigener Sache: wir pflegen bei ask! die "Liste Interessierte" mit E-Mail-Adressen für diesen Newsletter. Da wir die Anzahl der E-Mails über diese Liste gering halten wollen, bitte diese Liste NICHT für eigene Ankündigungs-Mails und Hinweise benutzen. Beiträge, die wir in einen Newsletter aufnehmen können, bitte gerne an {{< email "kontakt@ask-freiburg.net" >}} schicken, wir bündeln das dann im Newsletter-Format... vielen Dank!

So, jetzt kommt hier aber der nunmehr dritte Newsletter 2021, diesmal mit diesen Themen:

* Vermittlung von Netzwerkgesprächen im Raum Freiburg über ask!
* (GPV) Freiburg - Fachtag 2021 zum Thema Recovery am Mo, 04. Oktober
* Online-Konferenz zu Open Dialogue am 28./29. September
* Radiosendung ‘VielFalter’

### Vermittlung von Netzwerkgesprächen im Raum Freiburg über ask!

Derzeit gibt es im Raum Freiburg noch keine verbindlichen Angebote für Netzwerkgespräche. Es gibt allerdings schon einige Menschen, die für die Moderation von Netzwerkgesprächen mit der Methode des Offenen Dialogs ausgebildet sind. Und es werden während der gerade noch in Freiburg laufenden Ausbildung noch mehr.

Wir sind dabei, einen "Moderator:innen-Pool für Netzwerkgespräche" aufzubauen, mit dem es die Möglichkeit geben soll, Personen zu kontaktieren und anzufragen, die in der Moderation des Offenen Dialogs ausgebildet sind.

Bei Interesse an der Teilnahme in diesem "Moderator:innen-Pool NWG" als Moderator:in, oder bei Interesse an einem Netzwerkgespräch wendet euch bitte an {{< email "nwg-anfrage@ask-freiburg.net" >}}.

Mehr Informationen: [https://www.ask-freiburg.net/netzwerkgespraeche/](https://www.ask-freiburg.net/netzwerkgespraeche/ "https\://www.ask-freiburg.net/netzwerkgespraeche/")

### (GPV) Freiburg - Fachtag 2021 am Mo, 04. Oktober

Am Mo, 04. Oktober findet der Fachtag des Gemeindepsychiatrischen Verbunds Freiburg (GPV) unter dem Titel „Recovery – (K)ein Patentrezept?!“ statt. Anmeldungen sind bis 17.09.2021 erbeten, es folgt die Einladung vom GPV:

Nachdem der Fachtag des GPV  im letzten Jahr ausfallen musste, hoffen wir sehr darauf, dass das geplante Programm in diesem Jahr in Präsenz im Glashaus Rieselfeld stattfinden kann.

Unter dem Titel „Recovery – (K)ein Patentrezept?!“ wollen wir durch Vorträge und ein Worldcafe neue Einsichten in die Potentiale für die Menschen mit psychischen Beeinträchtigungen aber auch für Einrichtungen der (gemeinde)psychiatrischen Versorgung ermöglichen und Raum für Diskussionen bieten.

Daneben möchten wir aber auch die Vernetzung und den Austausch verschiedener Arbeitsfelder der Gemeindepsychiatrie unterstützen.

Anlagen:

* Anschreiben und Einladung
* Programm
* Anmeldebogen

Kontakt: Christine Kubbutat, (Freiburg Amt für Soziales und Senioren, Psychiatrie-Koordination)

### Online-Konferenz zu Open Dialogue am 28./29. September

Am 28./29. September findet eine online-Konferenz zum Thema "Offener Dialog" statt (englisch):

[https://opendialogueforpsychosis.com/#conference](https://opendialogueforpsychosis.com/#conference "https\://opendialogueforpsychosis.com/#conference")

### Radiosendung ‘VielFalter’

Die Radiosendung ‘VielFalter’ von Mirko auf Radio Dreyeckland ist inzwischen jeweils am ersten Montag des Monats von 20 bis 21 Uhr on  Air (Wiederholung tags drauf um 13 Uhr). Die Juli-Sendung 2021 findet  Ihr unter\
[https://vielfalter.podspot.de/post/sendung-vom-5-juli-2021-mit-karina-korecki-und-heike-petereit-zipfel/](https://vielfalter.podspot.de/post/sendung-vom-5-juli-2021-mit-karina-korecki-und-heike-petereit-zipfel/ "https://vielfalter.podspot.de/post/sendung-vom-5-juli-2021-mit-karina-korecki-und-heike-petereit-zipfel/") 
zum Herunterladen und Anhören.

*Hinweis: Kontakt-Informationen und Anlagen wurden aus der Website-Version des Newsletters entfernt.*