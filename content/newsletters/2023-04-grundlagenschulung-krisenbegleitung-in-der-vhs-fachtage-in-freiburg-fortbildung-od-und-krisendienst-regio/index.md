---
date: 2023-09-11T13:07:31.911Z
title: "2023-04: Grundlagenschulung Krisenbegleitung in der VHS, Fachtage in
  Freiburg, Fortbildung OD und Krisendienst Regio"
---
Hallo liebe ask!-Interessierte,

Hier kommt der vierte Newsletter des Jahres 2023, diesmal mit diesen Themen:

* Grundlagenschulung Krisenbegleitung in Freiburg ab Nov.: Anmeldung möglich
* DGSP-Fachtag Psychopharmaka in Freiburg am Fr, 22. Sept.
* Gemeinsamer Fachtag GPV Freiburg und Betreuungsbehörde - "Selbstbestimmung" am 25. Sept.
* Aktueller Stand Krisendienst Regio
* Fortbildung Offener Dialog: letzte Möglichkeit zur Anmeldung
* Termine

### Grundlagenschulung Krisenbegleitung in Freiburg ab Nov.: Anmeldung möglich

ask! e.V. organisiert eine Grundlagenschulung zum Thema “Krisenbegleitung” in 8 Modulen.\
\
Die Grundidee vom ehrenamtlichen ask! Krisenteam ist, dass es in einer Krise hilfreich sein kann, gesehen und verstanden zu werden, dass es hilft, wenn jemand in der Situation "da sein" kann. Das muss auch gar kein langjährig ausgebildeter Mensch sein, die wichtigste Expertise ist oft die eigene Lebens- und vielleicht auch Krisen-Erfahrung. Wir wollen uns als Krisenteam organisieren um genau das zu tun: für Menschen in Krisen da sein und Begleitung anbieten.\
Dabei sollen Fachkräfte, als Genesungsbegleiter*in ausgebildete Psychiatrieerfahrene, Angehörige und engagierte Bürgerinnen und Bürger zusammenkommen. Diese Grundlagenschulung soll als gemeinsame Basis dienen.

Das Projekt läuft auf ehrenamtlicher Basis unabhängig vom Projekt Krisendienst (siehe unten).\
\
Wann: 8 Module, beginnend am Sa, 11. November um 14:00 Uhr\
\
Wo: vhs im Colombi-Eck, Raum 12, Friedrichstr. 52, 79098 Freiburg\
\
Anmeldung und weitere Informationen: <https://vhs-freiburg.de/kurssuche/kurs/Schulung-Ausserstationaere-Krisenbegleitung-bei-ASK-eV/232500405#inhalt>\
\
Inhalte der Schulung:\
\
    Vorstellung des Konzepts\
    Bedürfnisangepasste Begleitung und Offener Dialog\
    Leitfaden für ambulante Krisenbegleitung des BPE\
    Recovery und Empowerment\
    Gewaltfreie Kommunikation\
    Deeskalationsstrategien\
    Gemeindepsychiatrisches Netzwerk der Region\
    Praktische Umsetzung des Projekts\
\
Flyer: <https://www.ask-freiburg.net/posts/grundlagenschulung-krisenbegleitung/vhs-schulung-flyer-single.pdf>

### DGSP-Fachtag Psychopharmaka in Freiburg am Fr, 22. Sept.

Am Fr, 22. September 2023 findet in Freiburg der 5. Fachtag des Fachausschusses Psychopharmaka der DGSP zum Thema "Psychosebegleitung und Neuroleptika" statt:\
"Über Möglichkeiten, in der Psychosebegleitung und -behandlung Neuroleptika möglichst niedrig zu dosieren und auf sie zu verzichten"

Auf dem Fachtag sind auch Mitglieder von ask! beim Programmpunkt "Offener Dialog und Krisenbegleitung" vertreten.

Anmeldung und Programm: <https://www.dgsp-ev.de/tagungen/aktuelle-tagungen-der-dgsp/fachtag-psychopharmaka.html>

### Gemeinsamer Fachtag GPV Freiburg und Betreuungsbehörde - "Selbstbestimmung" am 25. Sept.

Im Anhang sind die Unterlagen für den diesjährigen Fachtag des GPV Freiburg in Kooperation mit der Betreuungsbehörde, insbesondere das detaillierte Programm und der Anmeldebogen.

Das Thema des Fachtags ist "Selbstbestimmung von Menschen mit psychischen Beeinträchtigungen -- Chancen, Grenzen, Risiken" und er findet statt Montags von 10-16 Uhr im Glashaus Rieselfeld.

Wer Interesse hat über ask! teilzunehmen, bitte bis zum 15.09. unter Angabe der bevorzugten Klein-Gruppen anmelden und am besten auch kurz formlos bei kontakt(aet)ask-freiburg.net melden, damit wir etwas Überblick haben, wer über uns dabei ist.

### Aktueller Stand Krisendienst Regio

Beim Projekt "Freiburger Krisendienst" geht es kontinuierlich weiter: Die gemeinnützige Gesellschaft (mbH) "Regio Krisendienst" ist nun gegründet das Aufnahmeverfahren als Mitglied im Paritätischen läuft. Die Aufnahme ist Voraussetzung um den geplanten Förderantrag bei Aktion Mensch stellen zu können.

Ebenso läuft die Suche nach einer Leitungsstelle für die Geschäftsführung der "Regio Krisendienst" und es wurden auch schon weitere Fördermöglichkeiten für den Krisendienst ausgelotet.

Die "Regio Krisendienst" soll dann in Zukunft den Krisendienst ins Leben rufen und betreiben. Der Krisendienst soll zunächst mit einem kleinen Angebot beginnen und dann stufenweise immer weiter ausgebaut werden. Zunächst soll die telefonische Erreichbarkeit an Wochenenden und dann an immer mehr Abenden, Nächten und Tagen angeboten werden. Ein zentrales Element sollen aufsuchende multiprofessionelle Krisenteams und das Angebot von Netzwerkgesprächen mit "Offenem Dialog" sein.

Zusätzlich bieten Angebote wie die Krisenbegleitung von ask! (für die wir Mitstreiter*innen suchen und eine Krisenbegleitungs-Schulung organisieren -- siehe oben!), Selbsthilfegruppen, Freizeit- und kulturelle Angebote von z.B. FHG, Schwere(s)los, AK Leben usw. eine Infrastruktur, die in oder nach einer Krise unterstützen kann.

### Fortbildung Offener Dialog: letzte Möglichkeit zur Anmeldung

Der Start der Fortbildung "Offener Dialog" ab Oktober 2023 steht nun schon bald bevor. Es sind noch wenige Plätze frei und es wäre toll, wenn die noch belegt werden würden. Wer noch Menschen kennt, für die diese Fortbildung interessant sein könnte, bitte folgenden Link weitergeben:

<https://www.ask-freiburg.net/fortbildung-offener-dialog/>

### Termine

* Fr, 22.09. : Fachtag des Fachausschuss Psychopharmaka der DGSP e.V. in Freiburg, Thema: Psychosebegleitung und Neuroleptika <https://www.dgsp-ev.de/tagungen/aktuelle-tagungen-der-dgsp/fachtag-psychopharmaka.html>
* Mo, 25.09. 10-16 Uhr: Gemeinsamer Fachtag GPV Freiburg und Betreuungsbehörde - "Selbstbestimmung" --> ANMELDUNG erforderlich
* Mo, 25.09. 18:30: Trialog, in den Räumen der FHG/Club 55, Schwarzwaldstraße 9 im 2. OG.
* 10.-19.10. : Woche der Seelischen Gesundheit: <https://www.freiburg.de/pb/530411.html>