---
date: 2022-04-29T16:24:13.004Z
title: "2022-03: TanzNeuigkeiten Frühsommer 2022 (nächste Woche), ask!
  Netzwerktagung, Freiburger Trialog"
---
Hallo liebe ask!-Interessierte,

Hier kommt der dritte Newsletter des Jahres 2022, diesmal mit diesen Themen:

* TanzNeuigkeiten Frühsommer 2022
* ask! Netzwerktagung "Krisenbegleitung und Offener Dialog"
* Trialog Freiburg

## TanzNeuigkeiten Frühsommer 2022: 3 Angebote ab nächste Woche

Liebe Menschen, im Anhang ein Bewegungs/ Tanz-Kursangebot vom Recoverycollege mit meiner wärmsten Empfehlung (siehe Anhang).
Liebe Grüße, Hanna

Liebe*r Tanz- und Bewegungsbegeisterte*r,

> Nicht müde werden
> sondern dem Wunder
> leise wie einem Vogel
> die Hand hinhalten.
>
> \-- Hilde Domin --

Einatmen...Innehalten...Ausatmen...Im Körper sein...
Nächste Woche startet der neue Kurs "**Von Kopf bis Fuß" mit dem RCS - Recovery College Südbaden** hier in Freiburg. Dieser Kurs läuft online/draußen donnerstags von 17-18 Uhr über 7 Termine im Mai und Juni.
Mit dem Körper... in Bewegung... mit Tanz(improvisation), Bewegungsexploration, Vorstellungskräften, bodywork und der bodmemory-Methode.
Hier geht´s zur Website:
<https://sites.google.com/view/recoverycollege/rcs-angebot>

Freue ich mich auf dich!

Außerdem:
Im **TanzRaum.KörperZeit**, innerhalb des kunsttherapeutischen Projektes MuT - Muse und Therapie im Schwere(s)Los! Freiburg verlängert sich bis Ende Juli dieses Gruppenangebot:
Körperkräfte & MuT-Gedanken
Dies ist ein Angebot über 11 Termine, in denen mit der Kraft der Vorstellung (Imaginationsübungen) und Explorieren in Bewegung körpereigene Ressourcen angekurbelt sowie Alltagsauszeiten und Wohlbefinden ermöglicht werden dürfen.
Dazukommen kannst du nach einer kleinen Voranmeldung fortlaufend.

Mitmachen kannst du auch noch hier:
Beim Ring der Körperbehinderten am Seepark in Freiburg läuft seit Oktober 2021 der inklusive Tanzkurs **"\[mir geht´s RUNDum gut]"**.
Bewegung und Tanz treffen thematisch auf alles, was sich rund(um gut) anfühlt, kreist und dreht und ist offen für alle bewegungsinteressierten Menschen zu Fuß, auf Rollen oder Rädern ab 18 Jahren.
Dieser Kurs findet zweiwöchig dienstags von 17.30 Uhr bis 19.30 Uhr statt.
Genauere Termine, Infos und Anmeldemöglichkeiten gebe ich gerne raus.

Herzliche Grüße & freue mich Dich tanzend oder einfach so zu sichten,
Lisa

## ask! Netzwerktagung "Krisenbegleitung und Offener Dialog"

Noch 57 Tage bis zur ersten ask! Netzwerktagung "Krisenbegleitung und Offener Dialog" mit Ralf Bohnert, Volkmar Aderhold und Initiativen für Angebote des Offenen Dialogs. Die Anmeldung für die Tagung am Sa, 25. Juni läuft und es gibt noch freie Plätze:

<https://ask-freiburg.net/tagung/>

## Trialog Freiburg

Trialog ist keine therapeutische Methode, sondern eine neue Form der Beteiligungskultur. Im Trialog gehen Psychiatrieerfahrene, Angehörige und in der Psychiatrie Tätige als Experten in eigener Sache aufeinander zu, um voneinander zu lernen.

Der Freiburger Trialog trifft sich in der Regel jeden letzten Montag im Monat um 18.30 Uhr in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.

Nächster Termin: Mo, 30. Mai um 18:30 Uhr

Weitere Infos: <https://www.fhgev.de/gruppen/trialog-in-freiburg.html>

## Termine

* 1. Mai: ask!-Info-Stand in Freiburg auf dem Straßenfest im Grün
* 5./6. Mai: Hometreatment-Tagung in Bremen
* Fr, 06.05. von 14:00 bis 17:00 Uhr Tag der offenen Tür in der Ergänzenden Unabhängigen Teilhabeberatung (EUTB): Bugginger Straße 87, 79114 Freiburg
* 7. Mai: Tag der Inklusion auf der Landesgartenschau in Neuenburg: Infostand von ask!
* Mo, 30.05. 18:30: Trialog in den Räumen des Club 55, Schwarzwaldstraße 9 im 2. OG.
* 25. Juni: ask! Netzwerktagung "Krisenbegleitung und Offener Dialog" im Bürgerhaus Zähringen