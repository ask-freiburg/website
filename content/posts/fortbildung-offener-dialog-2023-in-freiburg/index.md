---
date: 2022-12-05T22:16:52.447Z
title: Fortbildung Offener Dialog 2023 in Freiburg
categories: Termine
---
ask! organisiert noch einmal eine Fortbildung zum Offenen Dialog mit Volkmar Aderhold in Freiburg:

* 9 Workshop-Termine
* Beginn: verschoben auf 25. Oktober 2023
* Ab sofort hier auf der Website: [Anmeldung und weitere Informationen](/fortbildung-offener-dialog)

<!--more-->
