---
date: 2020-10-07T12:06:18.414Z
title: Infoveranstaltung zum offenen Dialog in der FHG Oktober 2020
categories: Termine
image: Human-emblem-info-rectangle.png
---
**Wann:** Donnerstag 15. Oktober 2020 um 15:00 Uhr

**Wo:** In den Räumen der Freiburger Hilfsgemeinschaft e.V., Schwarzwaldstraße 9, 79117 Freiburg

**Beschreibung:** Die FHG und ask! stellen die Methode des "offenen Dialog" und auch die Möglichkeiten hier in Freiburg vor. Es wird Raum für Fragen geben.

<!--more-->
