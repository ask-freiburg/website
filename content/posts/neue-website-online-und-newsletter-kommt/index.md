---
date: 2020-12-20T18:36:41.045Z
title: Neue Website online und Newsletter kommt...
categories: Wissen
image: ask-logo.png
---
ask! hat ein neues – professionell gestaltetes – Logo bekommen und die ask!-Website wurde passend dazu überarbeitet. Heute, am 20.12.2020 geht unsere neue Website online.

Ab Januar 2021 soll es außerdem einen regelmäßigen [Newsletter](/newsletter-info) geben.

<!--more-->
