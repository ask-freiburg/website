---
date: 2025-01-10T21:51:42.176Z
title: Info-Abende zur Mitarbeit beim Regio-Krisendienst in der Region Freiburg
categories: Termine
aliases:
  - /posts/der-regio-krisendienst-kann-starten-do-21.11.-info-abend-mitarbeit
image: logo-krisendienst-regio.png
---
Wir brauchen motivierte Mitarbeiter\*innen für den entstehenden psychosozialen Krisendienst im Raum Freiburg!  Wir planen im April 2025 mit dem Telefondienst an den Wochenenden zu starten. Wenn Sie an einer freiberuflichen, bzw. ehrenamtlichen Mitarbeit beim Krisendienst als Honorarkraft interessiert sind und sich einen Telefondienst von 5 Stunden, inklusive Vor- und Nachbereitungszeit vorstellen können, dann würden wir Sie gerne an einem unserer **Info-Abende** begrüßen:

> **Donnerstag, 16. Januar 2025**\
> **17:30 – 19:30 Uhr**\
> in den Räumen der FHG e.V., Schwarzwaldstraße 9, 79117 Freiburg (2. OG)

und

> **Montag, 17. Februar 2025**\
> **17:30 – 19:30 Uhr**\
> in den Räumen der FHG e.V., Schwarzwaldstraße 9, 79117 Freiburg (2. OG)

Wir möchten Ihnen dort ein kurzes Update zum Krisendienst geben, zu den Rahmenbedingungen und Anforderungen an Mitarbeiter*innen, sowie uns (nochmals) kennenlernen.

Über eine formlose **Anmeldung** an {{< email "kontakt@regio-krisendienst.de" >}} würden wir uns freuen, damit wir besser planen können. (Gerne auch dort melden, wenn Sie generell an einer Mitarbeit interessiert sind, aber an dem Termin nicht teilnehmen können.)

{{< infobox >}}

<h3>EHRENAMT / HONORARKRÄFTE</h3>

<h4>{{< image src="logo-krisendienst-regio.png" alt="Logo vom Psychosozialen Krisendienst" width=200 width-mobile=150 margin="4" no-resize="" >}}</h4>

<h3 class="md:whitespace-nowrap">Wir suchen Mitarbeiter*innen für unser Krisentelefon!</h3>

<ul class="text-left">
<li><b>in den Abendstunden am Wochenende</b></li>
<li><b>Min. 1x pro Monat</b></li>
<li><b>Aufwandsentschädigung</b></li>
<li><b>Professionelle Begleitung</b></li>
<li><b>Arbeit im Tandem</b></li>
<li><b>Fortbildungen/Supervision</b></li>
</ul>

<p><em>Die Mitarbeiter*innen am Krisentelefon sollen <b>Kompetenzen im Umgang mit psychosozialen Krisen</b> mitbringen - als Professionelle, Betroffene, Angehörige, interessierte Bürger*innen</em></p>
{{< /infobox >}}

<!--more-->
