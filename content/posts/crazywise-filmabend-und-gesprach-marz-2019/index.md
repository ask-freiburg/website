---
date: 2019-02-27T23:00:00.000Z
title: "Crazywise: Filmabend und Gespräch März 2019"
categories: Termine
image: crazywise.png
---
**CRAZYWISE - ein Film von Phil Borges und Kevin Tomlinson**\
(Englisch mit deutschen Untertiteln, *Eintritt frei*)

**Wann:** Mittwoch 13. März 2019 um 19:00 Uhr

**Wo:** SUSI-Café SUSI-Bewohnerinnentreff Vaubanallee 2, Haltestelle Paula Modersohn Platz

**Beschreibung:** Filmabend und Gespräch zu Umgang mit Ver-rücktheit und Krisenbegleitung

**Über den Film:** ([www.crazywisefilm.com](https://www.crazywisefilm.com))

<!--more-->
Der amerikanische Fotograf Phil Borges findet in Stammesgesellschaften Menschen, die
psychische Krisen erleben. Durch ihr Anderssein werden sie zu Schamanen und Heilern. Aus
diesen Begegnungen entstand der Dokumentarfilm «Crazywise».
Adam hat schwere Krisen und fällt aus allen sozialen Netzen - bis er die Meditation
entdeckt. Ethaya überlebt mehrere Suizidversuche - und findet ein spirituelles Training, das
ihr Halt gibt. Der Fotograf Phil Borges folgt Menschen mit psychischen Krisen in den USA
und in indigenen Gesellschaften.
Was können wir von Menschen lernen, die in psychische Krisen geraten? Wie gehen
indigene Stammesgesellschaften mit solchen Menschen um? Der amerikanische Fotograf
und Menschenrechtsaktivist Phil Borges dokumentiert seit 25 Jahren das Leben indigener
Völker rund um den Erdball. Er erlebt, wie in diesen Kulturen psychotische Symptome als
Hinweise für besondere Sensibilität und Kräfte gedeutet werden, die der Gemeinschaft
zugute kommen können. Oft werden sie Schamanen genannt. Für den Film «Crazywise»
verknüpft er die Geschichten von Adam und Ethaya mit seinen Beobachtungen
schamanischer Traditionen in den Stammeskulturen. Lassen sich solche Erfahrungen auf
westliche Verhältnisse übertragen? Können Menschen mit psychischen Krisen selber Kräfte
mobilisieren, um anderen zu helfen? Eine faszinierende Reise in die weite Welt der
menschlichen Psyche.

> Veranstaltet von der SUSI-Sozialkoordination gemeinsam mit der Initiative für Außerstationäre Krisenbegleitung ask! e.V. (Freiburg)


*Bild: &copy; crazywisefilm.com*
