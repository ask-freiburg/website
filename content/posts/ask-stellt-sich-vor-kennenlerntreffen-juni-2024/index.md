---
date: 2024-06-02T17:26:39.121Z
title: ask! stellt sich vor --  Kennenlerntreffen Juni 2024
categories: Termine
image: human-emblem-info-rectangle.png
---
**Wann:** Mittwoch, 12.06.2024 um 17:00 Uhr

**Wo:** in den Räumen der Freiburger Hilfsgemeinschaft (FHG), Schwarzwaldstraße 9, 79117 Freiburg (2. OG)

**Beschreibung:** Treffen für alle Interessierten. Das Treffen richtet sich an Menschen, die erst seit kurzem oder noch gar nicht bei ask! dabei sind. Es wird um die Arbeit von ask! gehen und auch den entstehenden Krisendienst für die Region Freiburg und wie der Krisendienst mit der Arbeit von ask! zusammenhängt. 



<!--more-->

Wir geben einen Überblick über unsere Aktivitäten wie Krisenbegleitung, Krisendienst für Freiburg, Hintergrundinformationen zu unserem außerstationären Ansatz sowie Krisenverständnis und zeigen Möglichkeiten zur Mitwirkung bei ask! auf.

Interessieren Sie sich dafür Menschen in Krisensituationen zu unterstützen? Haben Sie selbst Erfahrungen (im Umgang) mit seelischen Krisen? Dann kommen Sie gerne unverbindlich zu diesem offenen Treffen!

Generell ist das Treffen interessant für alle Menschen (mit und ohne Psychiatrie-Erfahrung sowie Angehörige)

* die unsere Arbeit unterstützen möchten
* die KrisenbegleiterInnen werden möchten
* die sich ausführlicher über die Arbeit von ask! informieren möchten
* die Interesse haben, sich beim entstehenden Krisendienst für die Region Freiburg einzubringen (und ggf. auch schon an der zugehörigen Fortbildung im April teilgenommen haben)
