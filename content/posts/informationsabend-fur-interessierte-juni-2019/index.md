---
date: 2019-06-01T22:00:00.000Z
title: Informationsabend für Interessierte Juni 2019
categories: Termine
image: Human-emblem-info-rectangle.png
---
**Wann:** Donnerstag, 13.06.2019 um 18:00 Uhr

**Wo:** Treffpunkt Freiburg, Schwabentorring 2, 79098 Freiburg

**Beschreibung:** 
Treffen für alle Interessierten. Super, um mehr über ask! zu erfahren und Fragen beantwortet zu bekommen.

<!--more-->
Für Menschen (mit und ohne Psychiatrie-Erfahrung sowie Angehörige)

* die unsere Arbeit unterstützen möchten
* die KrisenbegleiterInnen werden möchten
* die sich ausführlicher über die Arbeit von ask! informieren möchten

Interessieren Sie sich dafür Menschen in Krisensituationen zu unterstützen? Haben Sie selbst Erfahrungen (im Umgang) mit seelischen Krisen? Dann kommen Sie gerne unverbindlich zu diesem offenen Treffen!
