---
date: 2021-08-22T22:00:00.000Z
title: Regio 2021 in Stuttgart
categories: Termine
image: regio2-200x150.png
---
Unter dem Motto

> Krisendienste – notwendiger denn je!\
> Moderne Konzepte der Krisenversorgung

fand am 2. Juli die **REGIO 2021 in Stuttgart** statt.

Die Aufzeichnung dieser Veranstaltung des Landesverbandes Gemeindepsychiatrie Baden-Württemberg e. V.\
und des Landesverbandes Psychiatrie-Erfahrener Baden-Württemberg e. V. ist abrufbar unter

<https://www.gemeindepsychiatrie-bw.de/2021/08/aufzeichnung-regio-2021/>

<!--more-->

*Bild REGIO-Logo: © Landesverband Gemeindepsychiatrie Baden-Württemberg e.V.*
