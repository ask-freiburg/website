---
date: 2023-09-01T20:23:00.069Z
title: Grundlagenschulung Krisenbegleitung
categories: Termine
image: vhs-schulung.png
---
Wir organisieren eine Grundlagenschulung zum Thema "Krisenbegleitung" in 8 Modulen!

**Wann:** 8 Module, beginnend am Sa, 11. November um 14:00 Uhr 

**Wo:** vhs im Colombi-Eck, Raum 12, Friedrichstr. 52, 79098 Freiburg

Anmeldung und weitere Informationen: [https://vhs-freiburg.de](https://vhs-freiburg.de/kurssuche/kurs/Schulung-Ausserstationaere-Krisenbegleitung-bei-ASK-eV/232500405#inhalt)

und im 

{{< filelink src="vhs-schulung-flyer-single.pdf" text="Flyer VHS-Schulung Krisenbegleitung" >}}

<!--more-->

Mit dem Projekt entwickeln wir einen praktischen Ansatz, Menschen in psychischen Krisen bedürfnisangepasst in ihrem vertrauten Wohnumfeld zu begleiten. Dafür sollen interessierte Fachkräfte, krisen- und psychiatrieerfahrene Menschen, Angehörige und Bürgerhelfer/-innen in einer Grundlagenschulung vorbereitet werden.

Inhalte der Schulung:

* Vorstellung des Konzepts
* Bedürfnisangepasste Begleitung und Offener Dialog
* Leitfaden für ambulante Krisenbegleitung des BPE
* Recovery und Empowerment
* Gewaltfreie Kommunikation
* Deeskalationsstrategien
* Gemeindepsychiatrisches Netzwerk der Region
* Praktische Umsetzung des Projekts