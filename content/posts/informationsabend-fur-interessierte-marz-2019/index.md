---
date: 2019-02-23T23:00:00.000Z
title: Informationsabend für Interessierte März 2019
categories: Termine
image: Human-emblem-info-rectangle.png
---
**Wann:** Freitag, 15.03.2019 um 18:00 Uhr

**Wo:** In den Räumen der Freiburger Hilfsgemeinschaft e.V., Schwarzwaldstraße 9, 79117 Freiburg

**Beschreibung:** 
Treffen für alle Interessierten. Super, um mehr über ask! zu erfahren und Fragen beantwortet zu bekommen.

<!--more-->
Interessieren Sie sich dafür Menschen in Krisensituationen zu unterstützen? Haben Sie selbst Erfahrungen (im Umgang) mit seelischen Krisen? Dann kommen Sie gerne unverbindlich zu diesem offenen Treffen!
