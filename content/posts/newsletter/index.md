---
date: 2021-01-24T18:49:04.344Z
title: Newsletter
categories: Wissen
image: Human-emblem-mail-rectangle.png
---
Heute wurde der [erste Newsletter](/newsletters/2021-01-neue-website-und-fortbildung-zum-offenen-dialog-in-freiburg/) mit folgenden Themen verschickt:

* Neue Website und neues Logo
* Schulung Offener Dialog und Netzwerkgespräche in Freiburg
* Weitere Themen und Termine

Er kann auf [dieser Website](/newsletter-info) nachgelesen werden. Infos zum Bezug des Newsletters per E-Mail gibt es dort auch.

<!--more-->
