---
date: 2019-01-01T14:39:48.001Z
title: "Zusammenfassung: Ereignisse vor 2019"
categories: Termine
---

Vor 2019 gibt es keine News-Einträge auf dieser Website. In diesem Eintrag fassen wir interessante Ereignisse vor dieser Zeit kurz zusammen.

<!--more-->

### 2018
* Durchführung einer VHS Schulung von Interessierten als Krisenhelfer\*in 
* regelmäßige Öffentlichkeitsarbeit mit Vorträgen

{{< image src="workshop2.jpg" alt="" width=300 width-mobile=250 margin=8 float="" >}}

### 2016
* Initiierung und Leitung des AK Krisendienst im Gemeindepsychiatrischen Verbund (GPV)

### 2015
* Gründung des gemeinnützigen Vereins; Aufnahme in den Gemeindepsychiatrischen Verbund (GPV)
* Initiierung des Fachtags "Ambulant und in Krisen begleiten"

### 2013
* Erhalt des Freiburger Fritz-Munder-Preises für die Initiative Außer-Stationäre-Krisenbegleitung

<https://www.regiotrends.de/de/regiomix/index.news.232701.fritz-munder-preis-stadt-freiburg-ehrt-diesjaehrige-preistraeger-in-einer-feierstunde-am-9.-april-in-der-gerichtslaube.html>

### 2012
* Erstes Zusammentreffen der Initiative ASK!
