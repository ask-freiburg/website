---
date: 2024-11-04T13:32:53.667Z
title: "Der Regio-Krisendienst kann starten - Mi, 20.11.: Info-Abend Mitarbeit"
categories: Termine
aliases:
  - /posts/der-regio-krisendienst-kann-starten-do-21.11.-info-abend-mitarbeit
image: logo-krisendienst-regio.png
---
Wir freuen uns sehr, dass die Finanzierung des Krisendienstes nun nach langer Geduldsprobe für die nächsten fünf Jahre gesichert ist.

Somit kann nun alles ins Laufen kommen!

Wir planen Anfang 2025 mit dem Telefondienst an den Wochenenden zu starten.

Dafür brauchen wir motivierte Mitarbeiter\*innen! Wenn Sie an einer freiberuflichen, bzw. ehrenamtlichen Mitarbeit beim Krisendienst als Honorarkraft interessiert sind und sich einen Telefondienst von 5 Stunden, inklusive Vor- und Nachbereitungszeit vorstellen können, dann würden wir Sie gerne an unserem **Info-Abend** begrüßen:

> **Mittwoch, 20. November 2024**\
> **17:00 – 19:00 Uhr**\
> in den Räumen der FHG e.V., Schwarzwaldstraße 9, 79117 Freiburg (2. OG)

Wir möchten Ihnen an diesem Abend ein kurzes Update zum Krisendienst geben, zu den Rahmenbedingungen und Anforderungen an Mitarbeiter*innen, sowie uns (nochmals) kennenlernen.

<!--more-->

Wir bitten um eine formlose **Anmeldung bis zum 15.11.** an {{< email "kontakt@regio-krisendienst.de" >}} wenn Sie an dem Info-Abend teilnehmen möchten. (Gerne auch dort melden, wenn Sie generell an einer Mitarbeit interessiert sind, aber an dem Termin nicht teilnehmen können.)


{{< infobox >}}
<h3>EHRENAMT / HONORARKRÄFTE</h3>

<h4>{{< image src="logo-krisendienst-regio.png" alt="Logo vom Psychosozialen Krisendienst" width=200 width-mobile=150 margin="4" no-resize="" >}}</h4>

<h3 class="md:whitespace-nowrap">Wir suchen Mitarbeiter*innen für unser Krisentelefon!</h3>

<ul class="text-left">
<li><b>in den Abendstunden am Wochenende</b></li>
<li><b>Min. 1x pro Monat</b></li>
<li><b>Aufwandsentschädigung</b></li>
<li><b>Professionelle Begleitung</b></li>
<li><b>Arbeit im Tandem</b></li>
<li><b>Fortbildungen/Supervision</b></li>
</ul>

<p><em>Die Mitarbeiter*innen am Krisentelefon sollen <b>Kompetenzen im Umgang mit psychosozialen Krisen</b> mitbringen - als Professionelle, Betroffene, Angehörige, interessierte Bürger*innen</em></p>
{{< /infobox >}}

Wir suchen außerdem nach einer zweiten Teilzeitstelle für die Koordinationsleitung des Krisendienstes (siehe {{< filelink src="Stellenausschreibung_leitung_regio-krisendienst.pdf" text="Stellenausschreibung" >}}):

{{< infobox >}}
<h3>LEITUNG KRISENDIENST</h3>

<h4>{{< image src="logo-krisendienst-regio.png" alt="Logo vom Psychosozialen Krisendienst" width=200 width-mobile=150 margin="4" no-resize="" >}}</h4>

<h3>Wir suchen eine Fachkraft mit Erfahrung im psychosozialen Bereich für die Leitung des Regio-Krisendienstes (50% Teilzeit)</h3>

<ul class="md:whitespace-nowrap text-left">
<li><b>Einsatzplanung der Honorar- und ehrenamtlichen Kräfte</b></li>
<li><b>Begleitung und fachliche Anleitung des Krisen-Teams</b></li>
<li><b>Organisationsentwicklung</b></li>
<li><b>Hoher Gestaltungsspielraum beim Aufbau eines neuen Krisendienstes</b></li>
<li><b>Vergütung analog zum TVöD SuE</b></li>
<li><b>Betriebliche Altersvorsorge</b></li>
</ul>
{{< /infobox >}}

{{< filelink src="Stellenausschreibung_leitung_regio-krisendienst.pdf" text="Stellenausschreibung Leitung Regio-Krisendienst" >}}
