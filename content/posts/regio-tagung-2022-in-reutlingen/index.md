---
date: 2022-06-27T22:32:48.542Z
title: Regio-Tagung 2022 in Reutlingen
categories: Termine
image: regio2-200x150.png
---
Unter dem Motto

> Mittendrin – doch außen vor 
> (Was heißt hier eigentlich Inklusion?)

fand am 9. Juli die **REGIO 2022 in Reutlingen** statt.

Weitere Informationen gibt es unter

<https://www.gemeindepsychiatrie-bw.de/2022/02/regio-2022/>

<!--more-->

*Bild REGIO-Logo: © Landesverband Gemeindepsychiatrie Baden-Württemberg e.V.*
