---
date: 2024-10-03T09:14:35.576Z
title: "Lesungen von Lea De Gregorio: Unter Verrückten sagt man Du"
categories: Termine
image: unter-verrueckten-sagt-man-du_cover.jpg
---
ask! organisiert im Rahmen der Woche der Seelischen Gesundheit zwei Lesungen mit Lea De Gregorio:

am **Sonntag 13. Oktober um 16:00 Uhr**\
in der **Mediathek Bad Krozingen** (Bahnhofsstraße 3b, 79198 Bad Krozingen)

und

am **Montag 14. Oktober um 19:30 Uhr**\
in der **Stadtbibliothek Freiburg** (Münsterplatz 17, 79098 Freiburg)

*Lea De Gregorio* liest aus Ihrem 2024 erschienenen Buch "**Unter Verrückten sagt man Du**":

<!--more-->

An einer Umbruchstelle im Leben wird Lea De Gregorio verrückt. Zu viele Gedanken drehen frei in ihrem Kopf, zu viele Fragen rasen ihr durchs Herz, der Schlaf bleibt aus. Und es folgt, was hierzulande nun mal vorgesehen ist: die Behandlung in der Psychiatrie. Doch geht der Heilung die Entmündigung voraus. Hier bestimmen, entscheiden, sprechen andere für sie.\
Muss sie sich dieser althergebrachten Ordnung tatsächlich fügen, damit alles besser wird? Oder sie erst recht in Frage stellen? Eine Suche nach grundlegenden Antworten beginnt, sie führt sie an tabuisierte Orte der Geschichte, in unsere Sprache, die Philosophie und schließlich in den Kampf. Gegen Ausgrenzung und Diskriminierung von Verrückten, einer viel zu lange übersehenen Minderheit.\
Lea De Gregorio entlarvt die tradierten Ungerechtigkeitenin unserem Denken, Fühlen, Handeln. Unter Verrückten sagt man du leistet dringend notwendige Psychiatrie- und Gesellschaftskritik. In einer Sprache, die so klar und so klug und so zärtlich ist, dass sie den Blick auf unser Zusammenleben substanziell zu verändern vermag.

{{< image src="degregorio_fr-bk.jpg" alt="Plaket zur Lesung mit Lea De Gregorio" width=0 width-mobile=0 float="" margin="4" no-resize="" >}}