---
date: 2019-03-03T16:45:08.782Z
title: Open Dialogue-Fortbildung in Freiburg 2019
categories: Termine
---
Es wird eine systemische Fortbildung in Familien- und Netzwerktherapie unter Leitung von Dr. Volkmar Aderhold in Freiburg angeboten.

Die Weiterbildung wird in Kooperation der Deutschen Gesellschaft für Soziale Psychiatrie (DGSP)-Landesverband Baden-Württemberg e.V. und mit dem Verein ask! Außerstationäre Krisenbegleitung e.V. ausgerichtet. Der Kurs startet im Mai 2019 und findet in bzw. bei Freiburg statt.

<!--more-->
**Alle weiteren Informationen und Anmeldung siehe:**

**[https://www.dgsp-ev.de/fortbildungen/weitere-fortbildungsangebote/open-dialog-fortbildungen/open-dialog-fortbildung-freiburg.htm](https://deref-web-02.de/mail/client/wfMcRnokPdQ/dereferrer/?redirectUrl=https%3A%2F%2Fwww.dgsp-ev.de%2Ffortbildungen%2Fweitere-fortbildungsangebote%2Fopen-dialog-fortbildungen%2Fopen-dialog-fortbildung-freiburg.html)**
