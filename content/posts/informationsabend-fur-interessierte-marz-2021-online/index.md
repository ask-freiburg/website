---
date: 2021-02-10T16:10:51.699Z
title: Treffen für Neue bei ask! -- März 2021 [online]
categories: Termine
image: human-emblem-info-rectangle.png
---
**Wann:** Freitag, 19.03.2021 um 18:00 Uhr

**Wo:** Online Video-Konferenz 

**Video-Konferenz-Zugang**: Zur Teilnahme an der BigBlue-Button-basierten Video-Konferenz wird ein Chromium-Basierter Browser empfohlen (z.B. [Iridium-Browser](https://iridiumbrowser.de/downloads/), [Chromium](https://chromium.woolyss.com/download/) oder Chrome) . Bitte unter {{< email "kontakt@ask-freiburg.net" >}} anmelden, um Zugangsdaten zu erhalten. 

**Beschreibung:** 
Treffen für alle Interessierten. Das Treffen richtet sich an Menschen, die erst seit kurzem oder noch gar nicht bei ask! dabei sind. Super, um mehr über ask! zu erfahren, Fragen beantwortet zu bekommen und rauszufinden, wie man sich bei ask! einbringen kann.

<!--more-->

Interessieren Sie sich dafür Menschen in Krisensituationen zu unterstützen? Haben Sie selbst Erfahrungen (im Umgang) mit seelischen Krisen? Dann kommen Sie gerne unverbindlich zu diesem offenen Treffen!

Generell ist das Treffen interessant für alle Menschen (mit und ohne Psychiatrie-Erfahrung sowie Angehörige)

* die unsere Arbeit unterstützen möchten
* die KrisenbegleiterInnen werden möchten
* die sich ausführlicher über die Arbeit von ask! informieren möchten