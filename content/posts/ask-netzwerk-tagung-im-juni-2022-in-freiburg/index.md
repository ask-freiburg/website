---
date: 2022-06-27T14:40:44.276Z
title: ask! Netzwerk-Tagung im Juni 2022 in Freiburg
categories: Termine
image: a1_ask-tagung-22.2.jpg
aliases:
  - /tagung
  - /tagung/anmeldung
---
Am 25. Juni 2022 fand in Freiburg die erste ask! Netzwerktagung statt. Die Themenschwerpunkte waren Krisenbegleitung und Offener Dialog – welches auch die beiden Hauptthemen sind, für die sich die Freiburger Initiative ask! seit nunmehr 10 Jahren einsetzt.

Hier stehen die Folien der Vorträge von Ralf Bohnert und Volkmar Aderhold und die archivierte Einladung zur Tagung zur Verfügung:

* {{< filelink src="ralf-bohnert_was-brauchen-menschen-in-krisen_2022-06-25_ask-netzwerktagung.pdf" text="Vortragsfolien Ralf Bohnert" >}}
* {{< filelink src="volkmar-aderhold_offener-dialog-und-netzwerkgespraeche_2022-06-25_ask-netzwerktagung_ohne-bilder.pdf" text="Vortragsfolien Volkmar Aderhold (ohne Bilder)" >}}
* {{< filelink src="ask-_6seiter_tagungen_web.pdf" text="Flyer: ask! Netzwerktagung" >}}

<!--more-->

\
\
\
_Wir bedanken uns herzlich für die finanzielle Förderung der Tagung bei:_

| GlücksSpirale        | [StuRa der Uni Freiburg](https://www.stura.uni-freiburg.de/) |
|:-------------:|:-------------:|
| {{< image src="logo-gluecksspirale.jpg" alt="Logo Glücksspirale" width=150 width-mobile=100 >}} | {{< image src="logo-stura.jpg" alt="Logo StuRa" width=150 width-mobile=100 >}} |
