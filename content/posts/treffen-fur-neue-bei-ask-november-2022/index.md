---
date: 2022-09-15T17:16:13.164Z
title: Treffen für Neue bei ask! -- November 2022
categories: Termine
image: human-emblem-info-rectangle.png
---
**Wann:** Mittwoch, 09.11.2022 um 19:00 Uhr

**Wo:** in den Räumen des Club 55, Schwarzwaldstraße 9, 79117 Freiburg

**Beschreibung:** 
Treffen für alle Interessierten. Das Treffen richtet sich an Menschen, die erst seit kurzem oder noch gar nicht bei ask! dabei sind. Super, um mehr über ask! zu erfahren, Fragen beantwortet zu bekommen und rauszufinden, wie man sich bei ask! einbringen kann.

<!--more-->

Interessieren Sie sich dafür Menschen in Krisensituationen zu unterstützen? Haben Sie selbst Erfahrungen (im Umgang) mit seelischen Krisen? Dann kommen Sie gerne unverbindlich zu diesem offenen Treffen!

Generell ist das Treffen interessant für alle Menschen (mit und ohne Psychiatrie-Erfahrung sowie Angehörige)

* die unsere Arbeit unterstützen möchten
* die KrisenbegleiterInnen werden möchten
* die sich ausführlicher über die Arbeit von ask! informieren möchten