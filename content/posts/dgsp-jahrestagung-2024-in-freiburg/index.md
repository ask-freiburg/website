---
date: 2024-07-15T22:02:32.334Z
title: DGSP Jahrestagung 2024 in Freiburg
categories: Termine
image: logomark.svg
---
Vom **14. bis 16. November** 2024 findet in Freiburg die **Jahrestagung der Deutschen Gesellschaft für Soziale Psychiatrie (DGSP)** statt, in der es um Fremdheit und Fremdheitserfahrungen und den Umgang mit eben diesen gehen soll.

Die Tagung wird zum ersten mal auch gemeinsam der Schweizerischen Gesellschaft für Sozialpsychiatrie (SOPSY) veranstaltet.

Weitere Informationen zum Programm und zur Anmeldung gibt es unter <https://www.dgsp-ev.de/veranstaltungen/aktuelle-tagungen/jahrestagung-2024-2>

<!--more-->