---
date: 2025-03-03T22:29:49.679Z
title: Regio-Tagung 2025 in Freiburg
categories: Termine
image: regio2-200x150.png
---
Die REGIO-Tagung findet dieses Jahr in Freiburg statt! Unter dem Motto 

> BTHG – Anspruch und Wirklichkeit\
> Was hat es gebracht?

finden Anfang April zahlreiche Vorträge und Workshops statt:

**Termin:** 4. April 2025, 9:30 – 17:00 Uhr
**Ort:** Katholische Akademie Freiburg, Wintererstraße 1, 79104 Freiburg

Weitere Informationen, Programm-Flyer und Anmeldung sind abrufbar unter

<https://www.gemeindepsychiatrie-bw.de/2025/01/regio-2025/>



<!--more-->