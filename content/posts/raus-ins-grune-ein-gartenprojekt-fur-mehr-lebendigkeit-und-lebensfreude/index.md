---
date: 2024-04-10T17:05:52.910Z
title: Raus .. ins Grüne! Ein Gartenprojekt für mehr Lebendigkeit und Lebensfreude
categories: Termine
image: oval-raus-ins-gruene.png
---
Das gartentherapeutische Projekt "Raus .. ins Grüne" von Katrin Birke findet ab Mitte Mai in Freiburg statt:

**Wann?**\
Mi, 22. + 29. Mai 2024 | jeweils 13.30 - 15.00 Uhr\
Mi, 19. + 26. Juni 2024 | jeweils 13.30 - 15.00 Uhr

**Wo?**\
Achillea – Freiburger Heilpflanzengarten e. V.\
Zechenweg 10, 79111 Freiburg-St. Georgen,\
<https://heilpflanzengarten.net/>

**Was kostet es?**\
Unkostenbeitrag: 25,- €\
Er enthält Materialkosten für Kräuterzubereitungen, Blumentöpfe, Erde, Samen od. Pflanzen und einen Obolus für die Nutzung des Heilpflanzengartens. Für meine Arbeit erhebe ich kein Honorar.

**Anmeldung** bitte bis 13. Mai 2024: raus-ins-gruene@gmx.de

Weitere Infos im Flyer: {{< filelink src="desktop-raus-ins-gruene.pdf" text="Flyer (PDF)" >}}

<!--more-->

{{< image src="oval-katrin.png" alt="Bild von Gartentherapeutin Katrin" width=291 width-mobile=200 float="right" margin="4" no-resize="" >}}

### Es ist eine wundervolle Zeit!

Überall blüht, grünt und duftet es. Die Tage werden länger und wärmer und sind erfüllt vom Gesang der Vögel. Lasst uns diese Zeit in der Natur hautnah erleben und genießen!

### Wo & Wie

In einer kleinen Gruppe von ca. fünf Personen treffen wir uns an vier Terminen in der einmaligen Atmosphäre des Heilpflanzengartens in FR-St. Georgen. Wild- und Küchenkräuter kennenlernen, in der Erde buddeln, leckere Kräutertees, -limonaden, und -essige herstellen, säen, pikieren, pflanzen und einen duftenden Kräutertopf für das Fensterbrett mit nach Hause nehmen - all das erwartet Sie bei diesem Angebot.

### Wer und warum?

Ich bin Katrin Birke und absolviere seit Okt. 2023 die Weiterbildung zur zertifizierten Gartentherapeutin in Grünberg, die ich im Sept. 2024 abschließen werde.
Das beschriebene Projekt ist Bestandteil meiner Abschlussarbeit, welches ich darüber hinaus gerne als festes Angebot speziell für Menschen mit Krisenerfahrungen etablieren möchte.\
Mein Anliegen ist es, den Teilnehmenden über den Umgang mit der Natur die Möglichkeit zu geben, (wieder) mehr Lebendigkeit und Lebensfreude zu erfahren und positive Impulse für den Alltag sowie die eigene Lebensgestaltung mitzunehmen.

### Was darf mitgebracht werden?

* Lust auf Grün :-) und Freude an der Beschäftigung mit Pflanzen und leichter Gartenarbeit zur Unterstützung des Heilpflanzengartens, der ausschließlich ehrenamtlich gepflegt wird
* die Bereitschaft, an allen vier Terminen teilzunehmen
* die Bereitschaft vor und nach dem Projekt einen kleinen Fragebogen anonym auszufüllen
* dem Wetter angepasste Kleidung, feste Schuhe, Sonnenschutz, Trinkflasche
* ggf. Schreibzeug für Notizen

**Organisatorisches**

* Der Unkostenbeitrag wird beim ersten Termin eingesammelt, bitte bar und möglichst passend mitbringen.
* Der Garten ist auch für Menschen im Rollstuhl zugänglich.
