---
date: 2024-02-29T17:24:09.172Z
title: Fortbildung Krisenintervention am 06. und 07. April 2024 mit Ralf Bohnert
categories: Termine
image: krisendienst_logo_final_website.jpg
---
Am **6. und 7. April** wird Ralf Bohnert, langjähriger Leiter des Krisendienstes Mittelfranken, zu einer weiteren **Fortbildung zum Thema "Krisenintervention"** nach Freiburg kommen.

Die Fortbildung richtet sich an Fachkräfte aus sozialen und sozial-psychiatrischen Arbeitsfeldern sowie Expert:innen aus Erfahrung mit einer EX-IN Qualifikation.

**Veranstaltungsort** der Fortbildung ist im Glashaus im Rieselfeld: Maria-von-Rudloff-Platz 2, 79111 Freiburg.

Die Fortbildung ist kostenlos und verbunden mit dem Wunsch, daß Sie potentiell bereit sind beim entstehenden Krisendienst mitzuarbeiten.

**Weitere Informationen und Anmeldung: {{< filelink src="fortbildung_ralf-bohnert-24_website.pdf" text="Einladung Fortbildung" >}}**

<!--more-->
