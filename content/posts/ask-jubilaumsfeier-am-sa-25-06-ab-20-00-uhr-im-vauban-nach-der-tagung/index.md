---
date: 2022-06-26T15:17:32.887Z
title: ask! Jubiläumsfeier am Sa, 25.06. ab 20:00 Uhr im Vauban (nach der Tagung)
categories: Termine
image: ask-jubilaeumsfeier.png
---
 Im Jahr 2022 feiert unsere Initative ask! Außerstationäre Krisenbegleitung ihr 10-jähriges Jubiläum! Zu diesem Anlass gab es am 25.06.2022 - im Anschluss an die ask! Netzwerktagung - eine Jubiläumsfeier im Haus037 im Vauban. 

<!--more-->

