# ask! e.V. Freiburg Website

Source code of <https://www.ask-freiburg.net>

## Getting started

1. Clone this repository
2. [Install Hugo](https://gohugo.io/getting-started/installing/)
3. Run `hugo server`

## CMS

This website uses [Netlify CMS](https://www.netlifycms.org/) to provide a web interace for editing content. See `static/admin/config.yml` for configuration details. To re-generate `static/admin-netlify-cms.js` run the following steps:

1. Install NodeJS and npm
2. Run `npm install`
3. Run `npm run build:cms` to generate the JavaScript file.

## Theming

This site uses [Tailwind CSS](https://tailwindcss.com) for styling. The CSS file in `assets/style.css` needs to be re-generated after changing CSS classes in `layouts/*.html` or changes to `src/style.css` as follows:

1. Install NodeJS and npm
2. Run `npm install`
3. Run `npm run build:css` to generate minimal CSS for production (commit changes).
4. Run `npm run build:css:dev` to generate CSS including all Tailwind CSS classes.

## License
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>

### Images
For copyright-information and sources of used images see <https://gitlab.com/ask-freiburg/website/-/tree/master/content/pages/impressum#bildnachweis>
