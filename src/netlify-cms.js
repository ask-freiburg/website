import CMS from 'netlify-cms'
CMS.registerEditorComponent({
  label: 'Datei',
  id: 'file_custom',
  fromBlock: match =>
    match && {
      file: match[1],
      text: match[3]
    },
  toBlock: ({ text, file, title }) =>
    `{{< filelink src="${file || ''}" text="${text || ''}" >}}`,
  toPreview: ({ text, file, title }, getAsset) => (
    `<a href={getAsset(file) || ''}>{text || file}</a>`
  ),
//  pattern: /^!\[(.*)\]\((.*?)(\s"(.*)")?\)$/,
  pattern: /^{{< filelink src="([^"]*)"(\stext="([^"]*)")? >}}$/,
  fields: [
    {
      label: 'Datei',
      name: 'file',
      widget: 'file',
      media_library: {
        allow_multiple: false,
      },
    },
    {
      label: 'Anzeige-Text',
      name: 'text',
    }
  ],
});
CMS.registerEditorComponent({
  label: 'Bild',
  id: 'image_custom',
  fromBlock: match =>
    match && {
      image: match[1],
      alt: match[3],
      width: match[5],
      widthmobile: match[7],
      float: match[9],
      margin: match[11],
      noresize: match[13]
    },
  toBlock: ({ alt, image, width, widthmobile, float, margin, noresize }) =>
    `{{< image src="${image || ''}" alt="${alt || ''}" width=${width || '0'} width-mobile=${widthmobile || '0'} float="${float  || ''}" margin="${margin  || ''}" no-resize="${noresize  || ''}" >}}`,
  toPreview: ({ alt, image, width, widthmobile, float, margin, noresize }, getAsset) => (
    `<img src={getAsset(image) || ''} alt={alt || ''} width={width} />`
  ),
  pattern: /^{{< image src="([^"]*)"(\salt="([^"]*)")?(\swidth=(\d*))?(\swidth-mobile=(\d*))?(\sfloat="([^"]*)")?(\smargin="([^"]*)")?(\sno-resize="([^"]*)")? >}}$/,
  fields: [
    {
      label: 'Bild',
      name: 'image',
      widget: 'image',
      media_library: {
        allow_multiple: false,
      },
    },
    {
      label: 'Breite',
      name: 'width',
      widget: 'number',
      valueType: 'int',
      min: 0
    },
    {
      label: 'Breite Smartphone',
      name: 'widthmobile',
      widget: 'number',
      valueType: 'int',
      min: 0
    },
    {
      label: 'Ausrichtung',
      name: 'float',
      widget: 'select',
      options: [ { label: "keine", value: "" }, { label: "links", value: "left" }, { label: "rechts", value: "right" } ],
      default: [""]
    },
    {
      label: 'Abstand',
      name: 'margin',
      widget: 'number',
      valueType: 'int',
      min: 0,
      default: 4
    },
    {
      label: 'Alt-Text',
      name: 'alt',
    },
    {
      name: 'noresize',
      widget: 'hidden',
      default: false
    }
  ],
});
/*
CMS.registerEditorComponent({
  label: 'Bild',
  id: 'image_custom_flyer',
  fromBlock: match =>
    match && {
      image: match[1],
      alt: match[3],
      width: match[5],
      widthmobile: match[7],
      float: match[9],
      margin: match[11],
      noresize: match[13]
    },
  toBlock: ({ alt, image, width, widthmobile, float, margin, noresize }) =>
    `{{< image src="${image || ''}" alt="${alt || ''}" width=${width || '0'} width-mobile=${widthmobile || '0'} float="${float  || ''}" margin="${margin  || ''}" no-resize="${noresize  || ''}" >}}`,
  toPreview: ({ alt, image, width, widthmobile, float, margin, noresize }, getAsset) => (
    `<img src={getAsset(image) || ''} alt={alt || ''} width={width} />`
  ),
  pattern: /^{{< image src="([^"]*)"(\salt="([^"]*)")?(\swidth=(\d*))?(\swidth-mobile=(\d*))?(\sfloat="([^"]*)")?(\smargin="([^"]*)")?(\sno-resize="([^"]*)")? >}}$/,
  fields: [
    {
      label: 'Bild',
      name: 'image',
      widget: 'image',
      media_library: {
        allow_multiple: false,
      },
    },
    {
      label: 'Breite',
      name: 'width',
      widget: 'number',
      valueType: 'int',
      min: 0
    },
    {
      label: 'Breite Smartphone',
      name: 'widthmobile',
      widget: 'hidden',
      valueType: 'int',
      min: 0
    },
    {
      label: 'Ausrichtung',
      name: 'float',
      widget: 'select',
      options: [ { label: "keine", value: "" }, { label: "links", value: "left" }, { label: "rechts", value: "right" } ],
      default: [""]
    },
    {
      label: 'Abstand',
      name: 'margin',
      widget: 'number',
      valueType: 'int',
      min: 0,
      default: 4
    },
    {
      label: 'Alt-Text',
      name: 'alt',
      widget: 'hidden'
    },
    {
      name: 'noresize',
      widget: 'hidden',
      default: true
    }
  ],
});
*/
CMS.registerEditorComponent({
  // Internal id of the component
  id: "youtube",
  // Visible label
  label: "Youtube",
  // Fields the user need to fill out when adding an instance of the component
  fields: [{name: 'id', label: 'Youtube Video ID', widget: 'string'}],
  // Pattern to identify a block as being an instance of this component
  pattern: /^{{< youtube (\S+) >}}$/,
  // Function to extract data elements from the regexp match
  fromBlock: function(match) {
    return {
      id: match[1]
    };
  },
  // Function to create a text block from an instance of this component
  toBlock: function(obj) {
    return '{{< youtube ' + obj.id + ' >}}';
  },
  // Preview output for this component. Can either be a string or a React component
  // (component gives better render performance)
  toPreview: function(obj) {
    return (
      '<img src="http://img.youtube.com/vi/' + obj.id + '/hqdefault.jpg" alt="Youtube Video"/>'
    );
  }
});
CMS.registerEditorComponent({
  // Internal id of the component
  id: "showmore",
  // Visible label
  label: "Show more",
  // Fields the user need to fill out when adding an instance of the component
  fields: [],
  // Pattern to identify a block as being an instance of this component
  pattern: /^(<!--more-->)$/,
  // Function to extract data elements from the regexp match
  fromBlock: function(match) {
    return {
      id: match[1]
    };
  },
  // Function to create a text block from an instance of this component
  toBlock: function() {
    return '<!--more-->';
  },
  // Preview output for this component. Can either be a string or a React component
  // (component gives better render performance)
  toPreview: function() {
    return (
      '---SHOW MORE---'
    );
  }
});
