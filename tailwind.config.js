module.exports = {
  content: ["./layouts/**/*.html", "./src/style.css", "./src/safelist.txt" ],
  theme: {
    extend: {
      colors: {
        logoPurple: '#2a1a61',
        logoYellow: '#dee327',
        logoYellowGreen: '#d5db40',
        logoYellowOrange: '#f2b838',
        logoYellowT: '#edeb82',
        logoGreen: '#a9c938',
        logoGreenT: '#c3d982',
        logoOrange: '#db9e27',
        logoOrangeT: '#fabe73',
      },
      fontFamily: {
          'sans': ['"Nimbus Sans L"','ui-sans-serif','system-ui','-apple-system','BlinkMacSystemFont','"Segoe UI"','Roboto','"Helvetica Neue"','Arial','"Noto Sans"','sans-serif','"Apple Color Emoji"','"Segoe UI Emoji"','"Segoe UI Symbol"','"Noto Color Emoji"']
      }
    }
  }
}
